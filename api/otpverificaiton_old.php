<?php

if(isset($_GET['action']) && $_GET['action'] == "send"){
    sendOTP();
}
if(isset($_GET['action']) && $_GET['action'] == "verify"){
    verifyOTP();
}
if(isset($_GET['action']) && $_GET['action'] == "resend"){
    resendOTP();
}
function sendOTP(){
$vars = array(
    'mobile' => $_GET['mobile'],
    'sender_id' => 'DGA101',
    'message' => 'Your digitalgurkha.com verification code is {code}',
    'expiry' => '900'
    );
$ch = curl_init();

curl_setopt($ch, CURLOPT_URL,"https://d7networks.com/api/verifier/send");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS,$vars);  //Post Fields
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$headers = array(
  "Authorization: Token c3844cfd0c407144ad5120446b4334a0e429c546",
);

curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

$server_output = curl_exec ($ch);

curl_close ($ch);

print  $server_output ;
//print json_encode(array('status'=>"open"));
}

function verifyOTP(){
$vars = array(
    'otp_id' => $_GET['otp_id'],
    'otp_code' => $_GET['otp_code']
    );
$ch = curl_init();

curl_setopt($ch, CURLOPT_URL,"https://d7networks.com/api/verifier/verify");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS,$vars);  //Post Fields
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$headers = array(
  "Authorization: Token c3844cfd0c407144ad5120446b4334a0e429c546",
);

curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

$server_output = curl_exec ($ch);

curl_close ($ch);

print $server_output;

//print json_encode(array('status'=>"success"));
}

function resendOTP(){
$vars = array(
    'otp_id' => $_GET['otp_id']
    );
$ch = curl_init();

curl_setopt($ch, CURLOPT_URL,"https://d7networks.com/api/verifier/resend");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS,$vars);  //Post Fields
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$headers = array(
  "Authorization: Token c3844cfd0c407144ad5120446b4334a0e429c546",
);

curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

$server_output = curl_exec ($ch);

curl_close ($ch);

print $server_output;

//print json_encode(array('status'=>"open","resend_count" => 2));
}