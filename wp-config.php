<?php

//Begin Really Simple SSL session cookie settings
@ini_set('session.cookie_httponly', true);
@ini_set('session.cookie_secure', true);
@ini_set('session.use_only_cookies', true);
//END Really Simple SSL
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', "digit662_web" );

/** MySQL database username */
define( 'DB_USER', "digit662_web" );

/** MySQL database password */
define( 'DB_PASSWORD', "6JEu+,gY&)3i" );

/** MySQL hostname */
define( 'DB_HOST', "localhost" );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'f9b3609881921e96bf5f59dbf63c13bfdf7cc6a6d14fc4805047a5040dfea423');
define('SECURE_AUTH_KEY', '08cbe8f98a4e942a75c5831dc57a5d9079b3a71f7a22d9ef8cf3df90fb9087d8');
define('LOGGED_IN_KEY', '0435aa2b7ccbc319e4daad721767b9855ddce18434e26acaa96610d533131c40');
define('NONCE_KEY', '35e835f010d0dd40026cbabd4919475b9a2982aab97e6f21f504ed66cdea96e8');
define('AUTH_SALT', '3f4d4f90f72b418ea73ac3505f54b10e0e8ac26778f970e3909faed05b68bf3c');
define('SECURE_AUTH_SALT', '51abd602505403aa41ae9a7b06ecc9b2a87e48a9488454edd430c8b242d140ee');
define('LOGGED_IN_SALT', '1c3e364256b80eb8625e7a2c75f23130dbff4a18c8f827d776b290859191f2e0');
define('NONCE_SALT', 'e447c9e0e44f1b0e713244e61864e21c8f891e22c943abbb0b05972f46bfed4c');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

define('FS_METHOD', 'direct');

/**
 * The WP_SITEURL and WP_HOME options are configured to access from any hostname or IP address.
 * If you want to access only from an specific domain, you can modify them. For example:
 *  define('WP_HOME','https://example.com');
 *  define('WP_SITEURL','https://example.com');
 *
*/

if ( defined( 'WP_CLI' ) ) {
    $_SERVER['HTTP_HOST'] = 'localhost';
}

define( 'WP_SITEURL', 'https://digitalgurkha.com' );
define( 'WP_HOME', 'https://digitalgurkha.com' );


/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';

define('WP_TEMP_DIR', ABSPATH . 'wp-content/');


//  Disable pingback.ping xmlrpc method to prevent Wordpress from participating in DDoS attacks
//  More info at: https://docs.bitnami.com/general/apps/wordpress/troubleshooting/xmlrpc-and-pingback/

if ( !defined( 'WP_CLI' ) ) {
    // remove x-pingback HTTP header
    add_filter('wp_headers', function($headers) {
        unset($headers['X-Pingback']);
        return $headers;
    });
    // disable pingbacks
    add_filter( 'xmlrpc_methods', function( $methods ) {
            unset( $methods['pingback.ping'] );
            return $methods;
    });
    add_filter( 'auto_update_translation', '__return_false' );
}

//post revision limitation
define( ‘WP_POST_REVISIONS’, 3 );

