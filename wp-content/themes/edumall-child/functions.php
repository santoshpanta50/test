<?php
defined( 'ABSPATH' ) || exit;

/**
 * Enqueue child scripts
 */
if ( ! function_exists( 'edumall_child_enqueue_scripts' ) ) {
	function edumall_child_enqueue_scripts() {
		wp_enqueue_style( 'edumall-child-style', get_stylesheet_directory_uri() . '/style.css' );
	}
}
add_action( 'wp_enqueue_scripts', 'edumall_child_enqueue_scripts', 15 );

// add_filter('tutor_student_registration_required_fields', 'required_phone_no_callback');
// if ( ! function_exists('required_phone_no_callback')){
//     function required_phone_no_callback($atts){
//         $atts['phone'] = 'Phone Number field is required';
//         return $atts;
//     }
// }
// add_action('user_register', 'add_phone_after_user_register');
// add_action('profile_update', 'add_phone_after_user_register');
// if ( ! function_exists('add_phone_after_user_register')) {
//     function add_phone_after_user_register($user_id){
//         if ( ! empty($_POST['phone'])) {
//             $phone_number = sanitize_text_field($_POST['phone']);
//             update_user_meta($user_id, '_phone_number', $phone_number);
//         }
//     }
// }