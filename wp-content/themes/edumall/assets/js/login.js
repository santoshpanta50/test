(
	function( $ ) {
		'use strict';

		var EdumallPopupPlugin = function( $el, options ) {
			this.ACTIVE_CLASS = 'open';

			this.init = function() {
				var plugin = this;

				$el.on( 'click', '.popup-overlay, .button-close-popup', function( e ) {
					e.preventDefault();
					e.stopPropagation();

					plugin.close();
				} );
			};

			this.open = function() {
				var plugin = this;

				$( '.edumall-popup' ).EdumallPopup( 'close' );

				$el.addClass( plugin.ACTIVE_CLASS );
			};

			this.close = function() {
				var plugin = this;

				$el.removeClass( plugin.ACTIVE_CLASS );
			};
		};

		$.fn.EdumallPopup = function( methodOrOptions ) {
			var method = (
				typeof methodOrOptions === 'string'
			) ? methodOrOptions : undefined;

			if ( method ) {
				var EdumallPopups = [];

				this.each( function() {
					var $el = $( this );
					var EdumallPopup = $el.data( 'EdumallPopup' );
					EdumallPopups.push( EdumallPopup );
				} );

				var args = (
					arguments.length > 1
				) ? Array.prototype.slice.call( arguments, 1 ) : undefined;

				var results = [];

				this.each( function( index ) {
					var EdumallPopup = EdumallPopups[ index ];

					if ( ! EdumallPopup ) {
						console.warn( '$.EdumallPopup not instantiated yet' );
						console.info( this );
						results.push( undefined );
						return;
					}

					if ( typeof EdumallPopup[ method ] === 'function' ) {
						var result = EdumallPopup[ method ].apply( EdumallPopup, args );
						results.push( result );
					} else {
						console.warn( 'Method \'' + method + '\' not defined in $.EdumallPopup' );
					}
				} );

				return (
					results.length > 1
				) ? results : results[ 0 ];
			} else {
				var options = (
					typeof methodOrOptions === 'object'
				) ? methodOrOptions : undefined;

				return this.each( function() {
					var $el = $( this );
					var EdumallPopup = new EdumallPopupPlugin( $el, options );

					$el.data( 'EdumallPopup', EdumallPopup );

					EdumallPopup.init();
				} );
			}
		};
	}( jQuery )
);

(
	function( $ ) {
		'use strict';

		$( document ).ready( function() {
			var messages = $edumallLogin.validatorMessages;

			jQuery.extend( jQuery.validator.messages, {
				required: messages.required,
				remote: messages.remote,
				email: messages.email,
				url: messages.url,
				date: messages.date,
				dateISO: messages.dateISO,
				number: messages.number,
				digits: messages.digits,
				creditcard: messages.creditcard,
				equalTo: messages.equalTo,
				accept: messages.accept,
				maxlength: jQuery.validator.format( messages.maxlength ),
				minlength: jQuery.validator.format( messages.minlength ),
				rangelength: jQuery.validator.format( messages.rangelength ),
				range: jQuery.validator.format( messages.range ),
				max: jQuery.validator.format( messages.max ),
				min: jQuery.validator.format( messages.min )
			} );

			var $body = $( 'body' );
			var $popupPreLoader = $( '#popup-pre-loader' );
			var $popupLogin = $( '#popup-user-login' );
			var $popupRegister = $( '#popup-user-register' );
			var $popupLostPassword = $( '#popup-user-lost-password' );


			$( '.edumall-popup' ).EdumallPopup();

			if ( $body.hasClass( 'required-login' ) && ! $body.hasClass( 'logged-in' ) ) {
				handlerLogin();
			}

			$body.on( 'click', '.open-popup-login', function( e ) {
				e.preventDefault();
				e.stopPropagation();

				handlerLogin();
			} );

			$body.on( 'click', '.open-popup-register', function( e ) {
				e.preventDefault();
				e.stopPropagation();

				handlerRegister();
			} );

			$body.on( 'click', '.open-popup-lost-password', function( e ) {
				e.preventDefault();
				e.stopPropagation();

				handlerLostPassword();
			} );

			$body.on( 'click', '.open-popup-instructor-register', function( e ) {
				e.preventDefault();
				e.stopPropagation();

				handlerInstructorRegister();
			} );
			
			$body.on('click','#verify-otp', function( e ){
			    e.preventDefault();
			    checkOTPVerification($('#ip_reg_phone').val());
			});
			
			$body.on('click', '#resend-otp', function ( e ){
			   e.preventDefault(); 
			   resendOTP($('#ip_reg_phone').val());
			});

			$body.on( 'click', '.btn-pw-toggle', function( e ) {
				e.preventDefault();
				e.stopPropagation();

				var groupField = $( this ).parent( '.form-input-password' );
				var pwField = groupField.children( 'input' );

				if ( 'password' === pwField.attr( 'type' ) ) {
					pwField.attr( 'type', 'text' );
					groupField.addClass( 'show-pw' );
				} else {
					pwField.attr( 'type', 'password' );
					groupField.removeClass( 'show-pw' );
				}
			} );

			function handlerLogin() {
				if ( $popupLogin.hasClass( 'popup-loaded' ) ) {
					$popupLogin.EdumallPopup( 'open' );
				} else {
					$.ajax( {
						url: $edumall.ajaxurl,
						type: 'GET',
						cache: false,
						dataType: 'html',
						data: {
							action: 'edumall_lazy_load_template',
							template: $popupLogin.data( 'template' )
						},
						success: function( response ) {
							$popupLogin.find( '.popup-content-inner' ).html( response );
							$popupLogin.addClass( 'popup-loaded' );
							$popupLogin.EdumallPopup( 'open' );

							// Remove inline css.
							$popupLogin.find( '.mo-openid-app-icons .mo_btn-social' ).removeAttr( 'style' );
							$popupLogin.find( '.mo-openid-app-icons .mo_btn-social .mofa' ).removeAttr( 'style' );
							$popupLogin.find( '.mo-openid-app-icons .mo_btn-social svg' ).removeAttr( 'style' );

							var $loginForm = $popupLogin.find( '#edumall-login-form' );
							$loginForm.validate( {
								rules: {
									user_login: {
										required: true
									},
									password: {
										required: true,
									}
								},
								submitHandler: function( form ) {
									var $form = $( form );
									var $submitButton = $form.find( 'button[type="submit"]' );

									if ( $submitButton.attr( 'disabled' ) === true ) {
										return false;
									}

									$submitButton.attr( 'disabled', true );

									$.ajax( {
										url: $edumall.ajaxurl,
										type: 'POST',
										cache: false,
										dataType: 'json',
										data: $form.serialize(),
										success: function( response ) {
											if ( ! response.success ) {
												$form.find( '.form-response-messages' ).html( response.messages ).addClass( 'error' ).show();
											} else {
												$form.find( '.form-response-messages' ).html( response.messages ).addClass( 'success' ).show();

												if ( '' !== response.redirect_url ) {
													window.location.href = response.redirect_url;
												} else {
													location.reload();
												}
											}
										},
										beforeSend: function() {
											$form.find( '.form-response-messages' ).html( '' ).removeClass( 'error success' ).hide();
											$form.find( 'button[type="submit"]' ).addClass( 'updating-icon' );
										},
										complete: function() {
											$form.find( 'button[type="submit"]' ).removeClass( 'updating-icon' ).attr( 'disabled', false );
										}
									} );
								}
							} );
						},
						error: function( MLHttpRequest, textStatus, errorThrown ) {
							console.log( errorThrown );
						},
						beforeSend: function() {
							$( '.edumall-popup' ).EdumallPopup( 'close' );
							$popupPreLoader.addClass( 'open' );
						},
						complete: function() {
							$popupPreLoader.removeClass( 'open' );
						}
					} );
				}
			}
			function resendOTP( phone ){
			    $.ajax({
				    url: "https://digitalgurkha.com/api/otpVerification.php",
				    type: 'GET',
				    dataType: 'json',
				    cors: true,
				    contentType: 'application/json',
				    data:{
				        "phone": window.localStorage.getItem('phone'),
				        "email": window.localStorage.getItem('email'),
				        "action": "resend"
				    },
				    success: function(res){
				        if(res.success == true){
				            if(res.resend_count <= 1) {
				                
				               // $('#timer').show();
						       // $('#resend-otp').hide();
				                
				            }else{
				                $('.otp-response-messages').html('<p style="color: red">Cannot resend otp more than once.</p>');
				            }
				        }else{
				            $('.otp-response-messages').html('<p style="color: red">Something went wrong</p>');
				        }
				    }
			        
			    });
			}
			function checkOTPVerification(phone) {
			      
				  var $otp = $('#ip_reg_otp').val();
				  var $form = $('#edumall-register-form');
				  $.ajax({
				    url: "https://digitalgurkha.com/api/otpVerification.php",
				    type: 'GET',
				    dataType: 'json',
				    cors: true,
				    contentType: 'application/json',
				    data:{
				        "phone": $form.find('[name="phone"]').val(),
				        "otp": $otp,
				        "email": $form.find('[name="email"]').val(),
				        "action": "verify"
				    },
				    success: function(res){
				        console.log(res);
				        if(res.success == true){
				           	var $form = $('#edumall-register-form');
							$.ajax( {
								url: $edumall.ajaxurl,
								type: 'POST',
								cache: false,
								dataType: 'json',
								data: $form.serialize(),
								success: function( response ) {
									if ( ! response.success ) {
										$form.find( '.form-response-messages' ).html( response.messages ).addClass( 'error' ).show();
									} else {
										$form.find( '.form-response-messages' ).html( response.messages ).addClass( 'success' ).show();
										location.reload();
									}
								},
								beforeSend: function() {
									//console.log("sdf");
									$form.find( '.form-response-messages' ).html( '' ).removeClass( 'error success' ).hide();
									$form.find( 'button[type="submit"]' ).addClass( 'updating-icon' );
								},
								complete: function() {
									$form.find( 'button[type="submit"]' ).removeClass( 'updating-icon' ).attr( 'disabled', false );
								}
							} );
									
				        }else{
				            $('.otp-response-messages').html('<p style="color: red">Something went wrong</p>');
				        }
				    }
				});
			}
			
			function startTimer(duration, display) {
                var timer = duration, minutes, seconds;
                setInterval(function () {
                    minutes = parseInt(timer / 60, 10);
                    seconds = parseInt(timer % 60, 10);
            
                    minutes = minutes < 10 ? "0" + minutes : minutes;
                    seconds = seconds < 10 ? "0" + seconds : seconds;
            
                    display.textContent = "OTP sent. Resending in " + minutes + ":" + seconds;
            
                    if (--timer < 0) {
                        timer = duration;
                        $('#timer').hide();
						$('#resend-otp').show();
                    }
                }, 1000);
            }

			function handlerRegister() {
				if ( $popupRegister.hasClass( 'popup-loaded' ) ) {
					$popupRegister.EdumallPopup( 'open' );
				} else {
					$.ajax( {
						url: $edumall.ajaxurl,
						type: 'GET',
						cache: false,
						dataType: 'html',
						data: {
							action: 'edumall_lazy_load_template',
							template: $popupRegister.data( 'template' )
						},
						success: function( response ) {
						   // console.log("df");
							$popupRegister.find( '.popup-content-inner' ).html( response );
							$popupRegister.addClass( 'popup-loaded' );
							$popupRegister.EdumallPopup( 'open' );

							var $registerForm = $popupRegister.find( '#edumall-register-form' );
							$registerForm.validate( {
								rules: {
									firstname: {
										required: true,
									},
									lastname: {
										required: true,
									},
									username: {
										required: true,
										minlength: 4,
									},
									email: {
										required: true,
										email: true
									},
									password: {
										required: true,
										minlength: 8,
										maxlength: 30
									},
									password2: {
										required: true,
										minlength: 8,
										maxlength: 30,
										equalTo: '#ip_reg_password',
									},
									phone: {
									    required: true,
									    maxlength: 10,
									    minlength: 10,
									}
								},
								submitHandler: function( form ) {
									var $form = $( form );
									var $submitButton = $form.find( 'button[type="submit"]' );
									var $phone = $form.find('[name="phone"]').val();
									var $verifybtn = $('#verify-otp');
									$.ajax({
									    url: "https://digitalgurkha.com/api/otpVerification.php",
									    type: 'GET',
									    dataType: 'json',
									    cors: true,
									    contentType: 'application/json',
									    data:{
									        "phone": $phone,
									        "email": $form.find('[name="email"]').val(),
									        "username": $form.find('[name="username"]').val(),
									        "password": $form.find('[name="password"]').val(),
									        "action": "send"
									    },
									    success: function(res){
									        console.log(res);
									        if(res.success == true){
									            $submitButton.attr( 'disabled', true );
									            window.localStorage.setItem('phone',$form.find('[name="phone"]').val());
									            window.localStorage.setItem('email',$form.find('[name="email"]').val());
									            $('#phone-field').hide();
									            $('#otp-field').show();
									            $('#resend-otp').hide();
									            $('#timer').show();
									            startTimer(60, document.querySelector('#timer'));
									        }
									    }
									});
								// 	$.ajax({
								// 	    url: "https://api.msg91.com/api/v5/otp",
								// 	    type: 'GET',
								// 	    dataType: 'json',
								// 	    cors: true ,
        //                                 contentType:'application/json',
        //                                 secure: true,
        //                                 headers: {
        //                                 'Access-Control-Allow-Origin': '*',
        //                                 'Content-Type': 'application/json',
        //                                 },
								// 	    data: {
								// 	        template_id: '617c0872608c4b111c2f819d',
								// 	        mobile: '+977'+$phone,
								// 	        authkey: '369003AocpCtEcEIot617c0fabP1'
								// 	    },
								// 	    success: function(res){
								// 	        console.log(res);
								// 	        if(res.type == "success"){
								// 	            $('#phone-field').hide();
								// 	            $('#op-field').show();
									          
								// 	        }
								// 	    }
								
								// 	});
					
									//return false;
				// 					$.ajax( {
				// 				url: $edumall.ajaxurl,
				// 				type: 'POST',
				// 				cache: false,
				// 				dataType: 'json',
				// 				data: $form.serialize(),
				// 				success: function( response ) {
				// 					if ( ! response.success ) {
				// 						$form.find( '.form-response-messages' ).html( response.messages ).addClass( 'error' ).show();
				// 					} else {
				// 						$form.find( '.form-response-messages' ).html( response.messages ).addClass( 'success' ).show();
				// 						location.reload();
				// 					}
				// 				},
				// 				beforeSend: function() {
				// 					//console.log("sdf");
				// 					$form.find( '.form-response-messages' ).html( '' ).removeClass( 'error success' ).hide();
				// 					$form.find( 'button[type="submit"]' ).addClass( 'updating-icon' );
				// 				},
				// 				complete: function() {
				// 					$form.find( 'button[type="submit"]' ).removeClass( 'updating-icon' ).attr( 'disabled', false );
				// 				}
				// 			} );
				
				// var form = new FormData();
    //             form.append("mobile", "9779840067403");
    //             form.append("sender_id", "SMSINFO");
    //             form.append("message", "Your otp code is {code}");
    //             form.append("expiry", "900");
									
    //                         $.ajax({
    //                           method: "POST",
    //                           url: "https://d7networks.com/api/verifier/send",
    //                           dataType: 'jsonp',
    //                           cors: true ,
    //                           contentType:'application/json',
    //                           secure: true,
    //                           headers: {
    //                              'Access-Control-Allow-Origin': '*',
    //                             'Content-Type': 'application/json',
    //                             'Authorization': 'Token c3844cfd0c407144ad5120446b4334a0e429c546'
    //                           },
    //                           data: {
    //                               mobile: "9779840067403",
    //                               sender_id: "DGA101",
    //                               message: "Your otp is {code}",
    //                               expiry: "900"
    //                           },
    //                           beforeSend: function (xhr) {
    //                               console.log("sent1");
    //         xhr.setRequestHeader ("Authorization", "Token c3844cfd0c407144ad5120446b4334a0e429c546");
    //       },
    //                           success: function (res){
    //                             console.log(res);
    //                           }
    //                         });
       
								}
							} );
						},
						error: function( MLHttpRequest, textStatus, errorThrown ) {
							console.log( errorThrown );
						},
						beforeSend: function() {
							$( '.edumall-popup' ).EdumallPopup( 'close' );
							$popupPreLoader.addClass( 'open' );
						},
						complete: function() {
							$popupPreLoader.removeClass( 'open' );
						}
					} );
				}
			}

			function handlerLostPassword() {
				if ( $popupLostPassword.hasClass( 'popup-loaded' ) ) {
					$popupLostPassword.EdumallPopup( 'open' );
				} else {
					$.ajax( {
						url: $edumall.ajaxurl,
						type: 'GET',
						cache: false,
						dataType: 'html',
						data: {
							action: 'edumall_lazy_load_template',
							template: $popupLostPassword.data( 'template' )
						},
						success: function( response ) {
							$popupLostPassword.find( '.popup-content-inner' ).html( response );
							$popupLostPassword.addClass( 'popup-loaded' );
							$popupLostPassword.EdumallPopup( 'open' );

							var $lostPasswordForm = $popupLostPassword.find( '#edumall-lost-password-form' );
							$lostPasswordForm.on( 'submit', function( e ) {
								e.preventDefault();

								var $form = $( this );
								var $submitButton = $form.find( 'button[type="submit"]' );

								if ( $submitButton.attr( 'disabled' ) === true ) {
									return false;
								}

								$submitButton.attr( 'disabled', true );

								$.ajax( {
									type: 'post',
									url: $edumall.ajaxurl,
									dataType: 'json',
									data: $form.serialize(),
									success: function( response ) {
										if ( ! response.success ) {
											$form.find( '.form-response-messages' ).html( response.messages ).addClass( 'error' ).show();
										} else {
											$form.find( '.form-response-messages' ).html( response.messages ).addClass( 'success' ).show();
										}
									},
									beforeSend: function() {
										$form.find( '.form-response-messages' ).html( '' ).removeClass( 'error success' ).hide();
										$form.find( 'button[type="submit"]' ).addClass( 'updating-icon' );
									},
									complete: function() {
										$form.find( 'button[type="submit"]' ).removeClass( 'updating-icon' ).attr( 'disabled', false );
									}
								} );
							} );
						},
						error: function( MLHttpRequest, textStatus, errorThrown ) {
							console.log( errorThrown );
						},
						beforeSend: function() {
							$( '.edumall-popup' ).EdumallPopup( 'close' );
							$popupPreLoader.addClass( 'open' );
						},
						complete: function() {
							$popupPreLoader.removeClass( 'open' );
						}
					} );
				}
			}

			function handlerInstructorRegister() {
				var $popup = $( '#edumall-popup-instructor-register' );

				if ( $popup.hasClass( 'popup-loaded' ) ) {
					$popup.EdumallPopup( 'open' );
				} else {
					$.ajax( {
						url: $edumall.ajaxurl,
						type: 'GET',
						cache: false,
						dataType: 'html',
						data: {
							action: 'edumall_lazy_load_template',
							template: $popup.data( 'template' )
						},
						success: function( response ) {
							$popup.find( '.popup-content-inner' ).html( response );
							$popup.addClass( 'popup-loaded' );
							$popup.EdumallPopup( 'open' );

							var $form = $popup.find( 'form' );
							$form.validate( {
								rules: {
									fullname: {
										required: true,
									},
									username: {
										required: true,
									},
									email: {
										required: true,
										email: true
									},
									password: {
										required: true,
										minlength: 8,
										maxlength: 30
									},
								},
								submitHandler: function( form ) {
									var $form = $( form );
									var $submitButton = $form.find( 'button[type="submit"]' );

									if ( $submitButton.attr( 'disabled' ) === true ) {
										return false;
									}

									$submitButton.attr( 'disabled', true );

									$.ajax( {
										url: $edumall.ajaxurl,
										type: 'POST',
										cache: false,
										dataType: 'json',
										data: $form.serialize(),
										success: function( response ) {
											if ( ! response.success ) {
												$form.find( '.form-response-messages' ).html( response.messages ).addClass( 'error' ).show();
											} else {
												$form.find( '.form-response-messages' ).html( response.messages ).addClass( 'success' ).show();

												if ( response.redirect ) {
													location.reload();
												}
											}
										},
										beforeSend: function() {
											$form.find( '.form-response-messages' ).html( '' ).removeClass( 'error success' ).hide();
											$form.find( 'button[type="submit"]' ).addClass( 'updating-icon' );
										},
										complete: function() {
											$form.find( 'button[type="submit"]' ).removeClass( 'updating-icon' ).attr( 'disabled', false );
										}
									} );
								}
							} );
						},
						error: function( MLHttpRequest, textStatus, errorThrown ) {
							console.log( errorThrown );
						},
						beforeSend: function() {
							$( '.edumall-popup' ).EdumallPopup( 'close' );
							$popupPreLoader.addClass( 'open' );
						},
						complete: function() {
							$popupPreLoader.removeClass( 'open' );
						}
					} );
				}
			}
		} );

	}( jQuery )
);
