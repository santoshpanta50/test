<?php
/**
 * Define constant
 */
$theme = wp_get_theme();

if ( ! empty( $theme['Template'] ) ) {
	$theme = wp_get_theme( $theme['Template'] );
}

if ( ! defined( 'DS' ) ) {
	define( 'DS', DIRECTORY_SEPARATOR );
}

define( 'EDUMALL_THEME_NAME', $theme['Name'] );
define( 'EDUMALL_THEME_VERSION', $theme['Version'] );
define( 'EDUMALL_THEME_DIR', get_template_directory() );
define( 'EDUMALL_THEME_URI', get_template_directory_uri() );
define( 'EDUMALL_THEME_ASSETS_DIR', get_template_directory() . '/assets' );
define( 'EDUMALL_THEME_ASSETS_URI', get_template_directory_uri() . '/assets' );
define( 'EDUMALL_THEME_IMAGE_URI', EDUMALL_THEME_ASSETS_URI . '/images' );
define( 'EDUMALL_THEME_SVG_DIR', EDUMALL_THEME_ASSETS_DIR . '/svg' );
define( 'EDUMALL_THEME_SVG_URI', EDUMALL_THEME_ASSETS_URI . '/svg' );
define( 'EDUMALL_FRAMEWORK_DIR', get_template_directory() . DS . 'framework' );
define( 'EDUMALL_CUSTOMIZER_DIR', EDUMALL_THEME_DIR . DS . 'customizer' );
define( 'EDUMALL_WIDGETS_DIR', get_template_directory() . DS . 'widgets' );
define( 'EDUMALL_PROTOCOL', is_ssl() ? 'https' : 'http' );
define( 'EDUMALL_IS_RTL', is_rtl() ? true : false );

define( 'EDUMALL_TUTOR_DIR', get_template_directory() . DS . 'framework' . DS . 'tutor' );
define( 'EDUMALL_FAQ_DIR', get_template_directory() . DS . 'framework' . DS . 'faq' );
define( 'EDUMALL_EVENT_MANAGER_DIR', get_template_directory() . DS . 'framework' . DS . 'event-manager' );

define( 'EDUMALL_ELEMENTOR_DIR', get_template_directory() . DS . 'elementor' );
define( 'EDUMALL_ELEMENTOR_URI', get_template_directory_uri() . '/elementor' );
define( 'EDUMALL_ELEMENTOR_ASSETS', get_template_directory_uri() . '/elementor/assets' );

/**
 * Load Framework.
 */
require_once EDUMALL_FRAMEWORK_DIR . '/class-functions.php';
require_once EDUMALL_FRAMEWORK_DIR . '/class-debug.php';
require_once EDUMALL_FRAMEWORK_DIR . '/class-aqua-resizer.php';
require_once EDUMALL_FRAMEWORK_DIR . '/class-performance.php';
require_once EDUMALL_FRAMEWORK_DIR . '/class-static.php';
require_once EDUMALL_FRAMEWORK_DIR . '/class-init.php';
require_once EDUMALL_FRAMEWORK_DIR . '/class-helper.php';
require_once EDUMALL_FRAMEWORK_DIR . '/class-global.php';
require_once EDUMALL_FRAMEWORK_DIR . '/class-actions-filters.php';
require_once EDUMALL_FRAMEWORK_DIR . '/class-kses.php';
require_once EDUMALL_FRAMEWORK_DIR . '/class-notices.php';
require_once EDUMALL_FRAMEWORK_DIR . '/class-popup.php';
require_once EDUMALL_FRAMEWORK_DIR . '/class-admin.php';
require_once EDUMALL_FRAMEWORK_DIR . '/class-compatible.php';
require_once EDUMALL_FRAMEWORK_DIR . '/class-customize.php';
require_once EDUMALL_FRAMEWORK_DIR . '/class-nav-menu.php';
require_once EDUMALL_FRAMEWORK_DIR . '/class-enqueue.php';
require_once EDUMALL_FRAMEWORK_DIR . '/class-image.php';
require_once EDUMALL_FRAMEWORK_DIR . '/class-minify.php';
require_once EDUMALL_FRAMEWORK_DIR . '/class-color.php';
require_once EDUMALL_FRAMEWORK_DIR . '/class-datetime.php';
require_once EDUMALL_FRAMEWORK_DIR . '/class-import.php';
require_once EDUMALL_FRAMEWORK_DIR . '/class-kirki.php';
require_once EDUMALL_FRAMEWORK_DIR . '/class-login-register.php';
require_once EDUMALL_FRAMEWORK_DIR . '/class-metabox.php';
require_once EDUMALL_FRAMEWORK_DIR . '/class-plugins.php';
require_once EDUMALL_FRAMEWORK_DIR . '/class-custom-css.php';
require_once EDUMALL_FRAMEWORK_DIR . '/class-templates.php';
require_once EDUMALL_FRAMEWORK_DIR . '/class-walker-nav-menu.php';
require_once EDUMALL_FRAMEWORK_DIR . '/class-sidebar.php';
require_once EDUMALL_FRAMEWORK_DIR . '/class-top-bar.php';
require_once EDUMALL_FRAMEWORK_DIR . '/class-header.php';
require_once EDUMALL_FRAMEWORK_DIR . '/class-title-bar.php';
require_once EDUMALL_FRAMEWORK_DIR . '/class-footer.php';
require_once EDUMALL_FRAMEWORK_DIR . '/class-post-type.php';
require_once EDUMALL_FRAMEWORK_DIR . '/class-post-type-blog.php';
require_once EDUMALL_FRAMEWORK_DIR . '/class-post-type-portfolio.php';

require_once EDUMALL_WIDGETS_DIR . '/class-widget-init.php';

require_once EDUMALL_TUTOR_DIR . '/class-tutor.php';

require_once EDUMALL_EVENT_MANAGER_DIR . '/class-event.php';

require_once EDUMALL_FRAMEWORK_DIR . '/class-post-type-zoom-meeting.php';

require_once EDUMALL_FAQ_DIR . '/main.php';

require_once get_template_directory() . DS . 'buddypress' . DS . '_classes/main.php';

require_once EDUMALL_FRAMEWORK_DIR . '/woocommerce/class-woo.php';
require_once EDUMALL_FRAMEWORK_DIR . '/class-content-protected.php';
require_once EDUMALL_FRAMEWORK_DIR . '/tgm-plugin-activation.php';
require_once EDUMALL_FRAMEWORK_DIR . '/tgm-plugin-registration.php';
require_once EDUMALL_FRAMEWORK_DIR . '/class-tha.php';

require_once EDUMALL_ELEMENTOR_DIR . '/class-entry.php';

//require_once get_template_directory() . DS . 'mobile' . DS . '/main.php';

/**
 * Init the theme
 */
Edumall_Init::instance()->initialize();

add_filter( 'woocommerce_add_to_cart_redirect', 'bbloomer_redirect_checkout_add_cart' );
 
function bbloomer_redirect_checkout_add_cart() {
   return wc_get_checkout_url();
}

add_filter( 'woocommerce_currencies', 'add_cw_currency' );
function add_cw_currency( $cw_currency ) {
     $cw_currency['Nepali Ruppees'] = __( 'Nepali Ruppees', 'woocommerce' );
     return $cw_currency;
}
add_filter('woocommerce_currency_symbol', 'add_cw_currency_symbol', 10, 2);
function add_cw_currency_symbol( $custom_currency_symbol, $custom_currency ) {
     switch( $custom_currency ) {
         case 'Nepali Ruppees': $custom_currency_symbol = 'Rs'; break;
     }
     return $custom_currency_symbol;
}
add_filter( 'woocommerce_get_price_html', 'bbloomer_price_free_zero', 9999, 2 );
   
function bbloomer_price_free_zero( $price, $product ) {
    if ( $product->is_type( 'variable' ) ) {
        $prices = $product->get_variation_prices( true );
        $min_price = current( $prices['price'] );
        if ( 0 == $min_price ) {
            $max_price = end( $prices['price'] );
            $min_reg_price = current( $prices['regular_price'] );
            $max_reg_price = end( $prices['regular_price'] );
            if ( $min_price !== $max_price ) {
                $price = wc_format_price_range( 'Free', $max_price );
                $price .= $product->get_price_suffix();
            } elseif ( $product->is_on_sale() && $min_reg_price === $max_reg_price ) {
                $price = wc_format_sale_price( wc_price( $max_reg_price ), 'Free' );
                $price .= $product->get_price_suffix();
            } else {
                $price = 'Free';
            }
        }
    } elseif ( 0 == $product->get_price() ) {
        $price = '<span class="woocommerce-Price-amount amount">Free</span>';
    }  
    return $price;
}
/**
 * Change a currency symbol
 */
add_filter('woocommerce_currency_symbol', 'change_existing_currency_symbol', 10, 2);

function change_existing_currency_symbol( $currency_symbol, $currency ) {
     switch( $currency ) {
          case 'NPR': $currency_symbol = 'Rs'; break;
     }
     return $currency_symbol;
}