<?php

namespace WPStaging\Backend\Pro\Upgrade;

use WPStaging\Backend\Optimizer\Optimizer;
use WPStaging\Backend\Pro\Notices\BackupsDifferentPrefixNotice;
use WPStaging\Core\Cron\Cron;
use WPStaging\Core\Utils\Logger;
use WPStaging\Core\Utils\Helper;
use WPStaging\Core\Utils\IISWebConfig;
use WPStaging\Core\Utils\Htaccess;
use WPStaging\Core\WPStaging;
use WPStaging\Framework\Staging\Sites;
use WPStaging\Pro\Backup\Service\BackupsFinder;

/**
 * Upgrade Class
 * This must be loaded on every page init to ensure all settings are 
 * adjusted correctly and to run any upgrade process if necessary.
 */
// No Direct Access
if( !defined( "WPINC" ) ) {
    die;
}

class Upgrade {

    /**
     * Previous Version number
     * @var string 
     */
    private $previousVersion;

    /**
     * Clone data
     * @var obj 
     */
    private $clones;

    /**
     * Global settings
     * @var type obj
     */
    private $settings;

    /**
     * Logger
     * @var obj 
     */
    private $logger;

    /**
     * db object
     * @var obj 
     */
    private $db;


    /**
     * @var Sites
     */
    private $stagingSitesHelper;

    public function __construct()
    {
        // Previous version
        $this->previousVersion = preg_replace('/[^0-9.].*/', '', get_option('wpstgpro_version'));

        // Options earlier than version 2.0.0
        $this->clones = get_option("wpstg_existing_clones", []);

        $this->settings = (object) get_option("wpstg_settings", []);

        $this->db = WPStaging::getInstance()->get("wpdb");

        // Logger
        $this->logger = new Logger();

        /** @var Sites */
        $this->stagingSitesHelper = WPStaging::make(Sites::class);
    }

    public function doUpgrade()
    {
        $this->upgrade2_0_3();
        $this->upgrade2_0_4();
        $this->upgrade2_1_7();
        $this->upgrade2_3_6();
        $this->upgrade2_6_5();
        $this->upgrade2_8_6();
        $this->upgrade4_0_3();
        $this->upgrade4_0_5();
        $this->setVersion();
    }

    /**
     * Move existing staging sites to new option defined in Sites::STAGING_SITES_OPTION
     */
    private function upgrade4_0_5()
    {
        if (version_compare($this->previousVersion, '4.0.5', '<')) {
            (new Sites())->upgradeStagingSitesOption();
        }
    }

    /**
     * To show notice if backups are created on version less than 4.0.3 but,
     * greater than 4.0.0
     */
    private function upgrade4_0_3()
    {
        // Previous version lower than 4.0.3
        if (version_compare($this->previousVersion, '4.0.3', '<') && version_compare($this->previousVersion, '4.0.0', '>=')) {
            /** @var BackupsFinder */
            $backupsFinder = WPStaging::make(BackupsFinder::class);
            $backups = $backupsFinder->findBackups();
            // Only enable the notice if any backup available
            if (count($backups) > 0) {
                (new BackupsDifferentPrefixNotice())->enable();
            }
        }
    }

    /**
     * Fix array keys of staging sites
     */
    private function upgrade2_8_6()
    {
        // Previous version lower than 2.8.6
        if (version_compare($this->previousVersion, '2.8.6', '<')) {

            // Current options
            $sites = $this->stagingSitesHelper->tryGettingStagingSites();

            $new = [];

            // Fix keys. Replace white spaces with dash character
            foreach ($sites as $oldKey => $site) {
                $key       = preg_replace("#\W+#", '-', strtolower($oldKey));
                $new[$key] = $sites[$oldKey];
            }

            if (!empty($new)) {
                $this->stagingSitesHelper->updateStagingSites($new);
            }
        }
    }

    private function upgrade2_6_5()
    {
        // Previous version lower than 2.6.5
        if (version_compare($this->previousVersion, '2.6.5', '<')) {
            // Add htaccess to wp staging uploads folder
            $htaccess = new Htaccess();
            $htaccess->create(trailingslashit(WPStaging::getContentDir()) . '.htaccess');
            $htaccess->create(trailingslashit(WPStaging::getContentDir()) . 'logs/.htaccess');

            // Add litespeed htaccess to wp root folder
            if (extension_loaded('litespeed')) {
                $htaccess->createLitespeed(ABSPATH . '.htaccess');
            }

            // create web.config file for IIS in wp staging uploads folder
            $webconfig = new IISWebConfig();
            $webconfig->create(trailingslashit(WPStaging::getContentDir()) . 'web.config');
            $webconfig->create(trailingslashit(WPStaging::getContentDir()) . 'logs/web.config');
        }
    }

    /**
     * Upgrade method 2.0.3
     */
    public function upgrade2_0_3()
    {
        // Previous version lower than 2.0.2
        if (version_compare($this->previousVersion, '2.0.2', '<')) {
            $this->upgradeOptions();
            $this->upgradeClonesBeta();
            $this->upgradeNotices();
        }
    }

    /**
     * Upgrade method 2.0.4
     */
    public function upgrade2_0_4()
    {
        if ($this->previousVersion === false || version_compare($this->previousVersion, '2.0.5', '<')) {

            // Register cron job.
            $cron = new Cron();
            $cron->scheduleEvent();

            // Install Optimizer 
            $optimizer = new Optimizer();
            $optimizer->installOptimizer();
        }
    }

    /**
     * Upgrade method 2.1.7
     * Sanitize the clone key value.
     */
    private function upgrade2_1_7()
    {
        if ($this->previousVersion === false || version_compare($this->previousVersion, '2.1.7', '<')) {

            $sites = $this->stagingSitesHelper->tryGettingStagingSites();

            foreach ($sites as $key => $value) {
                unset($sites[$key]);
                $sites[preg_replace("#\W+#", '-', strtolower($key))] = $value;
            }

            if (!empty($sites)) {
                $this->stagingSitesHelper->updateStagingSites($sites);
            }
        }
    }

    /**
     * Upgrade method 2.3.6
     */
    private function upgrade2_3_6()
    {
        // Previous version lower than 2.3.6
        if (version_compare($this->previousVersion, '2.3.6', '<')) {
            $this->upgradeElements();
        }
    }

    /**
     * Add missing elements
     */
    private function upgradeElements()
    {
        // Current options
        $sites = $this->stagingSitesHelper->tryGettingStagingSites();

        if ($sites === false || count($sites) === 0) {
            return;
        }

        // Check if key prefix is missing and add it
        foreach ($sites as $key => $value) {
            if (empty($sites[$key]['directoryName'])) {
                continue;
            }

            !empty($sites[$key]['prefix']) ?
                            $sites[$key]['prefix'] = $value['prefix'] :
                            $sites[$key]['prefix'] = $this->getStagingPrefix($sites[$key]['directoryName']);
        }

        if (!empty($sites)) {
            $this->stagingSitesHelper->updateStagingSites($sites);
        }
    }

    /**
     * Check and return prefix of the staging site
     * @param string $directory
     * @return string
     */
    private function getStagingPrefix($directory)
    {
        // Try to get staging prefix from wp-config.php of staging site
        $path = ABSPATH . $directory . "/wp-config.php";

        if (($content = @file_get_contents($path)) === false) {
            $prefix = "";
        } else {
            // Get prefix from wp-config.php
            preg_match("/table_prefix\s*=\s*'(\w*)';/", $content, $matches);

            $prefix = "";
            if (!empty($matches[1])) {
                $prefix = $matches[1];
            }
        }

        // return result: Check if staging prefix is the same as the live prefix
        if ($this->db->prefix !== $prefix) {
            return $prefix;
        }

        return "";
    }

    /**
     * Upgrade routine for new install
     */
    private function upgradeOptions()
    {
        // Write some default vars
        add_option('wpstg_installDate', date('Y-m-d h:i:s'));
        $this->settings->optimizer = 1;
        update_option('wpstg_settings', $this->settings);
    }

    /**
     * Write new version number into db
     * return bool
     */
    private function setVersion()
    {
        // Check if version number in DB is lower than version number in current plugin or if it contains a deprecated development version number like 2021.07.22.162834673
        if (version_compare($this->previousVersion, WPStaging::getVersion(), '<') || $this->isInvalidVersionNumber()) {
            // Update Version number
            update_option('wpstgpro_version', preg_replace('/[^0-9.].*/', '', WPStaging::getVersion()));
            // Update "upgraded from" version number
            update_option('wpstgpro_version_upgraded_from', preg_replace('/[^0-9.].*/', '', $this->previousVersion));

            return true;
        }

        return false;
    }

    /**
     * During development, we've added non-semver version numbers like 2021.07.22.162834673
     * These numbers prevent the proper execution of the upgrade process because version_compare() does not work on such numbers.
     * We have to delete and invalidate these non-semver numbers before executing the upgrade process.
     *
     * @return bool
     */
    private function isInvalidVersionNumber(){
        if (strpos($this->previousVersion, '2021', 0) === 0) {
            return true;
        }
        return false;
    }

    /**
     * Create a new db option for beta version 2.0.2
     */
    private function upgradeClonesBeta()
    {
        if (empty($this->clones) || count($this->clones) === 0) {
            return;
        }

        $existingClones = $this->stagingSitesHelper->tryGettingStagingSites();
        $helper         = new Helper();

        // Add old clones to existing clones
        foreach ($this->clones as $key => &$value) {

            // Skip the rest of the loop if data is already compatible to wpstg 2.0.2
            if (isset($value['directoryName']) || !empty($value['directoryName'])) {
                continue;
            }

            // Skip if clone is already copied
            if (isset($existingClones[$value])) {
                continue;
            }

            $existingClones[$value]['directoryName'] = $value;
            $existingClones[$value]['path']          = get_home_path() . $value;
            $existingClones[$value]['url']           = $helper->getHomeUrl() . "/" . $value;
            $existingClones[$value]['number']        = $key + 1;
            $existingClones[$value]['version']       = $this->previousVersion;
        }

        unset($value);

        if (empty($existingClones) || $this->stagingSitesHelper->updateStagingSites($existingClones) === false) {
            $this->logger->log('INFO', 'Failed to upgrade clone data from ' . $this->previousVersion . ' to ' . WPStaging::getVersion());
        }
    }

    /**
     * Upgrade Notices db options from wpstg 1.3 -> 2.0.1
     * Fix some logical db options
     */
    private function upgradeNotices()
    {
        $poll   = get_option("wpstg_start_poll", false);
        $beta   = get_option("wpstg_hide_beta", false);
        $rating = get_option("wpstg_RatingDiv", false);

        if ($poll && $poll === "no") {
            update_option('wpstg_poll', 'no');
        }

        if ($beta && $beta === "yes") {
            update_option('wpstg_beta', 'no');
        }

        if ($rating && $rating === 'yes') {
            update_option('wpstg_rating', 'no');
        }
    }
}
