=== WP STAGING PRO - DB & File Duplicator & Migration  ===

Author URL: https://wordpress.org/plugins/wp-staging
Plugin URL: https://wordpress.org/plugins/wp-staging
Contributors: ReneHermi, WP-Staging
Donate link: https://wp-staging.com/#pricing
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags: backup, database backup, staging, duplication, clone
Requires at least: 3.6+
Tested up to: 5.8.1
Stable tag: 4.0.5
Requires PHP: 5.5

A duplicator plugin - clone/move, duplicate & migrate websites to staging, backup and development sites that only authorized users can access.

== Description ==

<h3>WP STAGING for WordPress Migration & Cloning </h3>
This duplicator, staging and backup plugin can create an exact copy of your entire website in seconds.* Great for staging, backup or development purposes.
(Cloning and Backup time depends on the size of your website)<br /><br />
This clone plugin creates a clone of your website into a subfolder or subdomain (Pro) of your main WordPress installation. The clone includes an entire copy of your database.
 <br /> <br />
<strong>Note: </strong> For pushing & migrating plugins and theme files to production site, check out the pro edition [https://wp-staging.com/](https://wp-staging.com/ "WP STAGING PRO")
<br /><br />
All the time-consumptive database and file cloning operations are done in the background. The plugin even automatically does an entire search & replace of all serialized links and paths.
 <br /><br />
This staging and backup plugin can clone your website even if it runs on a weak shared hosting server.
 <br /><br />
 <br /><br />
WP STAGING can help you to prevent your website from being broken or unavailable because of installing untested plugin updates!

[youtube https://www.youtube.com/watch?v=Ye3fC6cdB3A]

= Main Features =

* WPSTAGING clones the whole production site into a subfolder like example.com/staging-site.
* Easy to use! Create a clone of your site by clicking one button "CREATE NEW STAGING SITE".
* No Software as a Service - No account needed! All data belong to you only and stay on your server.
* No server timeouts on huge websites or/and small hosting servers
* Very fast - Migration and clone process takes only a few seconds or minutes, depending on the website's size and server I/O power.
* Use the clone as part of your backup strategy
* Only administrators can access the clone website. (Login with the same credentials you use on your production site)
* SEO friendly: The clone website is unavailable to search engines due to a custom login prompt and gets the meta entry no-index.
* The admin bar on the staging website is orange colored and shows clearly when you work on the staging site.
* Extensive logging features
* Supports all popular web servers: Apache, Nginx, and Microsoft IIS
* <strong>[Premium]: </strong>Choose a separate database and select a custom directory for cloning
* <strong>[Premium]: </strong>Make the clone website available from a subdomain like dev.example.com
* <strong>[Premium]: </strong>Push & migrate entire clone site inc. all plugins, themes, and media files to production website.
* <strong>[Premium]: </strong>Define user roles that get access to the clone site only. For instance, clients or external developers.
* <strong>[Premium]: </strong>Migration and cloning of WordPress multisites

> Note: Some features are Premium. Which means you need WP STAGING Pro to use those features. You can [get WP STAGING Premium here](https://wp-staging.com)!

* New: Compatible with WordFence & All In One WP Security & Firewall

= Additional Features WP STAGING PRO Edition  =

* Cloning and migration of WordPress multisites
* Define a separate database and a custom directory for cloning
* Clone your website into a subdomain
* Specify certain user roles for accessing the staging site
* Copy all modifications from staging site to the production website

<strong>Change your workflow of updating themes and plugins data:</strong>

1. Use WP STAGING to clone a production website for staging, testing or backup purposes
2. Create a backup of your website
3. Customize theme, configuration, update or install new plugins
4. Test everything on your staging site and keep a backup of the original site
5. If everything works on the staging site start the migration and copy all modifications to your production site!

<h3> Why should I Use a Staging Website? </h3>

Plugin updates and theme customizations should be tested on a staging platform first before they are done on your production website.
It's recommended having the staging platform on the same server where the production website is located to use the same hardware and software environment for your test website and to catch all possible errors during testing.

Before you update a plugin or going to install a new one, it is highly recommended to check out the modifications on a clone of your production website.
This makes sure that any modifications work on your production website without throwing unexpected errors or preventing your site from loading. Better known as the "WordPress blank page error".

Testing a plugin update before installing it in a production environment isn´t done very often by most users because existing staging solutions are too complex and need a lot of time to create a
an up-to-date copy of your website.

You may be afraid of installing plugins updates because you follow the rule "never touch a running system" with having in mind that untested updates are increasing the risk of breaking your site.
This is one of the main reasons why WordPress installations are often outdated, not updated at all and insecure because of this non-update behavior.

<strong> It's time to change this, so use "WP STAGING" for cloning, backup and migration of WordPress websites</strong>

<h3> Can´t I just use my local wordpress development system like xampp / lampp for testing and backup purposes? </h3>

You can test your website locally but if your local hardware and software environment is not a 100% exact clone of your production server there is NO guarantee that every aspect of your local copy is working on your production website exactly as you expect it.
There are some obvious things like differences in the config of PHP and the server you are running but even such non-obvious settings like the amount of RAM or the CPU performance can lead to unexpected results on your production website.
There are dozens of other possible cause of failure which can not be handled well when you are testing your changes on a local platform only without creating a backup staging site.

This is were WP STAGING jumps in... Site cloning, backup and staging site creation simplified!

<h3>I just want to migrate the database from one installation to another</h3>
If you want to migrate your local database to an already existing production site you can use a tool like WP Migrate DB.
WP STAGING is intended for creating a staging site with latest data from your production site or creating a backup of it. So it goes the opposite way of WP Migrate DB.
Both tools are excellent cooperating each other.

<h3>What are the benefits compared to a plugin like Duplicator?</h3>
I really the Duplicator plugin. It is a great tool for migrating from a development site to production one or from production site to development one and a good tool to create a backup of your WordPress website.
The downside is that before you can even create an export or backup file with Duplicator a lot of adjustments, manually interventions and requirements are needed before you can start the backup process.
Duplicator also needs some skills to be able to create a backup and development/staging site, where WP STAGING does not need more than a click from you.
Duplicator is best placed to be a tool for first-time creation of your production site. This is something where it is very handy and powerful.

If you have created a local or web-hosted development site and you need to migrate this site the first time to your production domain than you are doing nothing wrong with using
the Duplicator plugin! If you need all your latest production data like posts, updated plugins, theme data and styles in a testing environment or want to create a quick backup before testing out omething than I recommend to use WP STAGING instead!

= Can I give You some Feedback? =
This plugin has been created in thousands of hours and works even with the smallest shared web hosting package.
We also use enterprise level approved testing coding structures to make sure that the plugin runs rock solid on your system.
If you are a developer you will probably like to hear that we use Codeception and PHPUnit for our software.

As there are infinite numbers of possible server constellations it still might happen that something does not work for you 100%. In that case,
please open a [support request](https://wp-staging.com/support/ "support request") and describe your issue.


= Important =

Permalinks are disabled on the staging / backup site after first time cloning / backup creation
[Read here](https://wp-staging.com/docs/activate-permalinks-staging-site/ "activate permalinks on staging site") how to activate permalinks on the staging site.



= How to install and setup? =
Install it via the admin dashboard and to 'Plugins', click 'Add New' and search the plugins for 'WP STAGING'. Install the plugin with 'Install Now'.
After installation, go to the settings page 'Staging' and do your adjustments there.


== Frequently Asked Questions ==

* What is the Difference between WP STAGING and a Regular Backup Plugin?
You may have heard about other popular backup plugins like BackWPUp, BackupWordPress, Simple Backup, WordPress Backup to Dropbox or similar WordPress backup plugins and now wonder about the difference between WP STAGING and those backup tools.
Other backup plugins usually create a backup of your WordPress filesystem and a database backup which you can use to restore your website in case it became corrupted or you want to go back in time to a previous state.
The backup files are compressed and can not be executed directly. WP STAGING on the other hand creates a full backup of the whole file system and the database in a working state that you can open like your original production website.

Even though WP STAGING comes with some backup capabilities it's main purpose is to create a clone of your website which you can work on. It harmonies very well with all the mentioned backup plugins above and we recommend that you use it in conjunction with these backup plugins.

Note, that some free backup plugins are not able to support custom tables. (For instance the free version of Updraft plus backup plugin). In that case, your backup plugin is not able to create a backup of your staging site when it is executed on the production site.
The reason is that the tables created by WP STAGING are kind of custom tables beginning with another table prefix.
To bypass this limitation and to be able to create a backup of your staging site, you can setup your backup plugin on the staging site and create the backup from that location. This works well with every available WordPress backup plugin.

* I can not log in to the staging / backup site
If you are using a security plugin like All In One WP Security & Firewall you need to install the latest version of WP STAGING to access your cloned backup site.
Go to WP STAGING > Settings and add the slug to the custom login page which you set up in All In One WP Security & Firewall plugin.



== Official Site ==
https://wp-staging.com

== Installation ==
1. Download the file "wp-staging.zip":
2. Upload and install it via the WordPress plugin backend wp-admin > plugins > add new > uploads
3. Activate the plugin through the 'Plugins' menu in WordPress.

== Official Site ==
https://wp-staging.com

== Installation ==
1. Download the file "wp-staging-pro.zip":
2. Upload and install it via the WordPress plugin backend wp-admin > plugins > add new > uploads
3. Activate the plugin through the 'Plugins' menu in WordPress.
4. Start Plugins->Staging

== Screenshots ==

1. Step 1. Create new WordPress staging site
2. Step 2. Scanning your website for files and database tables
3. Step 3. Wordpress Staging site creation in progress
4. Finish!

== Changelog ==

= 4.0.5 =
* Fix: New pro version does not recognize staging sites created with older free version #1293

= 4.0.4 =
* Enh: Refactor the wp_login action hook to work with different parameter count than the one in WordPress Core #1223
* Enh: Sort new staging sites in descending order by creation time #1226
* Enh: Warn if creating a backup in PHP 32 bit version #1231
* Enh: Update the backup upload success message #1221
* Enh: Show a notice if there is a new WP STAGING free version #1250
* Enh: Rename db option wpstg_existing_clones_beta to wpstg_staging_sites #1211
* Enh: Update the warning message shown when the delete process of the staging site fails #1257
* Enh: Allow use of REST API on staging sites without login #1287
* Enh: Add new EDD software licensing updater #1294
* Fix: Fix a rare issue that could happen when creating a new staging site or restoring a backup when there is a row with primary key with value zero #1271
* Fix: Try to repair MyISAM table if it's corrupt when creating a Backup #1222
* Fix: Fix an issue on backup creation that would cause a database export to loop when encountering a table with negative integers or zeros as a primary key value #1251
* Fix: Lock specific tables while exporting a backup, to prevent a rare duplicated row issue #1245
* Fix: If the memory exhausts during a database export using the Backup feature, lower the memory usage/speed of the export and try again automatically #1230
* Fix: Prevent failure of adding database to backup from causing a loop #1231
* Fix: Fix issue when old clones from version 1.1.6 or lower replaces the existing clones from later version when upgrading from FREE to PRO version #1233
* Fix: Fix inconsistent Unselect All Tables button's action #1243
* Fix: Replace undefined date with proper formatted date during backups for some warning and critical messages #1244
* Fix: Split file scanning of wp-content into scanning of plugins, themes, uploads and other directories to reduce timeout issues #1247
* Fix: Rename .user.ini to .user.ini.bak after cloning to reduce fatal errors on staging site. Also show a notice. #1255
* Fix: Skip scanning the root directory if all other directories are unselected #1256
* Fix: Show correct insufficient space message instead of permission error if unable to copy due to insufficient space #1283
* Fix: Fix showing of error when unable to count tables rows and wrap table name when fetching rows during backup #1285
* Fix: Remove custom error handler that could cause errors due to notices being thrown #1292
* Fix: Fix an error that would occur when PHP lacked permission to get the size of a directory when pushing a staging site to production #1295
* Dev: Set the version of Selenium containers to 3.141.59-20210713 to avoid issues with broken latest version of selenium #1234

= 4.0.3 =
* New: Support for WordPress 5.8
* New: Show notice if uploads dir is outside WP Root #1138
* Enh: Show warning during restore if Backup was created on a server with PHP ini "short_open_tags", and restoring on a server with it disabled #1129
* Enh: Also show disabled permalink message in disabled items notice on the staging site and show a page builder (DIVI, Elementor etc) not working help link in wpstg page footer #1150
* Enh: Allow filtering the Backup directory using the `wpstg.backup.directory` filter #1167
* Enh: Decouple clone name and clone ID for better usage #1158
* Enh: Allow backups on the staging site #1172
* Enh: Show issue notice if backups is created on version >= 4.0.2 #1198
* Enh: Remove deprecated hooks call #1209
* Fix: Fix staging site when site has freemius script #1112
* Fix: Prefix 'wpstg' to sweetalerts Swal to avoid conflict with its other versions #1125
* Fix: Fix a bug in the backup export logic that would loop when encountering a file with non-empty contents that PHP would evaluate as false #1126
* Fix: Set default values for wpstg settings on plugin activate event if wpstg settings not already set #1135
* Fix: Fix the problem when unable to access the staging site because production site have different siteurl or home url and either one of them is having www. prefix #1136
* Fix: Restore a backup with VIEWs or TABLEs with special MySQL configurations such as DEFINER #1139
* Fix: Fix issue where tab triangle was inconsistent by using css based tab triangle #1148
* Fix: Fix issue where backup tmp folder cleaning process closes logs modal #1157
* Fix: Reduce time to query INFORMATION_SCHEMA on some shared hosts from ~10s to one millisecond #1154
* Fix: Fix a bug on backup creation that would not prefix the table name if a MySQL View is selecting data from another MySQL View #1155
* Fix: Fix a bug on backup restore that would fail when trying to create a MySQL View that selects data from another MySQL View that has not been created yet due to order of creation #1155
* Fix: Check available free disk space on large disks on 32-bit PHP #1179
* Fix: Fix a bug where a PHP memory_limit of -1 (Unlimited) would be interpreted as 64MB, now it's interpreted as 512MB #1178
* Fix: Fix download of .wpstg files on Bitnami/AWS Lightsail servers #1181
* Fix: Remove usages of `abstract static` methods that would violate `strict` PHP checks #1185
* Fix: Cloning a site resets the settings to the default ones #1183
* Fix: Fix a bug on backup creation that would prevent user from logging in after restoring a backup on a site with a different WPDb prefix #1169
* Fix: Allow backup restore with a warning if file count is different than expected, improve backup file count logic #1189
* Fix: Fix Clone RESET and Clone DELETE when unable to delete file due to permission error #1196
* Fix: Fix an issue when canceling a push confirm redirects to empty page #1206
* Fix: Add missing back button and hide cancel button after clone UPDATE and clone RESET #1207
* Fix: Fix Error in JS console related to registering of main-menu in page where it was not available #1205
* Dev: Add wrapper methods for deprecated hooks functions to support WordPress < 4.6 #1209


= 4.0.2 =
* Enh: Replace css based hover with pure js hoverintent for tooltips #1106
* Enh: Cleanup logs older than 7 days automatically #1116
* Enh: Update the version to check in Outdated WP Staging Hooks notice #1118
* Fix: Fixed conflict with Rank Math Seo PRO when Rank Math Seo PRO is activated network wide in multisites #1111
* Fix: Make Scan::hasFreeDiskSpace() return other info even if disk_free_space is unavailable #1093
* Fix: Fix an issue where MySQL views were not being deleted correctly during the database cleanup step of the backup export #1098

= 4.0.1 =
* Enh: Delete Optimizer Plugin on WP Staging plugins deactivate instead of uninstall #1096
* Enh: Optimize compatibility to restore backup generated in newer versions of MySQL #1109
* Enh: More robust backup upload by allowing a small size difference before considering it invalid #1110
* Fix: Replace the deprecated of calling a non-static method in daily version check hooks #1092
* Fix: Fixed an issue where a column with reserved MySQL keywords would cause a malformed MySQL query during the backup export #1097
* Fix: Try catch all instance of directory iterators #1101
* Dev: Refactor the JS code of the Backup feature for better readability and maintainability #1102

= 4.0.0 =
* New: You can now Backup, Download and Restore your entire website anytime you want! Easily migrate your website to another server by uploading the Backup and Restoring it on another server! #746
* Enh: Schedule the uploads backup to be deleted after one week if that option was selected during push #980
* Enh: Allow copying of only that symlink whose source is a directory #979
* Enh: Show notice only to user who can manage_options if wp_options table is missing primary key #1009
* Enh: Gracefully handle disk full during backup creation/restore #1041
* Fix: Handle error properly for Filesystem::delete() method #974
* Fix: Remove loading wpstg scripts as ESM to allow loading them as asynchronous #1007
* Fix: Properly handle exception while cleaning themes and plugins bak and tmp directories #1017
* Fix: Delete the clone even if in any case a corrupted delete job cache file existed for delete job #1033
* Fix: No cloning/pushing logs were written to file. Now fixed. #1040
* Fix: Wrap wp_doing_ajax in a adapter and use that adapter to call it to make it usable in WP < 4.7  #1047
* Fix: Fix typo and wrap up text in i18n for src/Backend/views/clone/ajax/start.php #1051
* Fix: Fix missing clone options warning during scanning process for old clones for UPDATE and RESET #1058
* Fix: Make isExcludedDirectories condition works for relative directories path too #1054
* Fix: Allow backup to be exported/restored from/to sites with custom WordPress directory structures #1032
* Fix: Set donation link to redirect to WP Staging pricing page #1080
* Dev: Add a shortcut to allow to use the DI container as a Service Locator easier under some circumstances #1039
* Dev: Add trait to allow for easier use of the `uopz` extension in tests #1053
* Dev: Replace const related tests logic with UOPZ for better readability and control #1079

= 3.2.6 =
* Feat: Compatible up to WordPress 5.7.2
* Enh: Preserve directories/tables selection and excludes rules for RESET and UPDATE process #809
* Enh: If any wpstg process is running allow to stop that process within RESET modal #942
* Enh: Properly show error message if unable to scan a directory (especially for Windows IIS env) #960
* Fix: Fix multisite subsite capabilities on the staging site #852
* Fix: Properly resets the properties between Cloning tasks #896
* Fix: Avoid PHP warning when building version of missing asset file #929
* Fix: Clean leftover wpstg-tmp-* and wpstg-bak-* directories from plugins and themes directories while push #954
* Fix: Make RESET modal show error message within modal on failed response instead of browser logs #942
* Fix: Replace wpstgGetCloneSettings() in mail-settings.php with CloneOption::get() #956
* Fix: Little typo changed effect to affect #963
* Fix: Made node_modules dir to be only excluded from WP Staging's Plugins #963
* Fix: Fix UPDATE and RESET for old wpstg clones which are without appended underscore db prefix #958
* Fix: Always use database table prefix in lowercase for Windows environment #967

= 3.2.5 =
* Feat: Compatible up to WordPress 5.7.1 #855
* Feat: Check disk space according to selected directories #761
* Feat: Make the staging site admin bar background color customizable #758
* Feat: Add UI to exclude certain files or folders by rules #726
* Feat: Show success popup on UPDATE and RESET jobs' completion #818
* Enh: Disallow invalid character in table prefix #819
* Enh: Directory navigation in file selection to infinite deep level #768
* Enh: Remove dependency on Symfony libraries #888
* Fix: Fix copy of big external tables #795
* Fix: Exclude mainsite uploads dir content while cloning non-main sites in multisite #755
* Fix: Fix RESET process for destination dir outside WP Root #808
* Fix: Exclude blog_versions table from SearchReplace job as well #807
* Fix: Fix issue where sub directories can only be collapsed when parent directory is checked #835
* Fix: Push now works if the ANSI_QUOTES sql_mode is enabled, as it is by default on Digital Ocean #839
* Fix: Fix isStagingSiteCloneable index not found when saving settings on old staging site #846
* Dev: Add Queue and Background Processing first layer of support #743
* Fix: Fix: Showing warning alert during PUSH when user tries to navigate to other page or close the browser/tab #848
* Fix: Fix unable to connect external database when making sure staging site doesn't use production site database and prefix #851
* Fix: Database backup's DELETE now works when deleting backup with no table #857
* Fix: Fix cloning not working due to warnIfClosingDuringProcess not being called properly #871
* Dev: Integrated Rollup for bundling/minifying/concatenating assets #828
* Dev: Remove console.log() output #874

= 3.2.4 =
* Enh: Add Shutdownable interface to replace usages of __destruct in the code #729
* Enh: Refactor on how the plugin keeps track of a request running time #766
* Fix: Replace deprecated jQuery click method #730
* Fix: Fix overlapping of sweetalert confirmation on push with sidebar #742
* Fix: Exclude wp staging content folder during staging #741
* Fix: Add sanitizing for path to fix comparing for Windows paths #751
* Fix: _cerber_files tables are excluded and could not be copied Fix #770
* Fix: Replaced jQuery assignment with an IIFE wrapper #761
* Fix: Fix extra directories scanning #779
* Fix: Wrap a few error_log calls in WPSTG_DEBUG to avoid cluttering the log #783
* Fix: Improve symlink consistency and add better error logs for symlink #786
* Fix: Fix extra directories check if an empty needle is provided to strpos in it #788
* Dev: Update php-scoper and other development dependencies #744
* Dev: Build javascript when building the distributable version of the plugin #750
* Dev: Internal helper CLI command to order the changelog notes according to type #749
* Dev: Refactor Job(s) implementation to use the Resources Trait #765
* Dev: Add internal documentation to versioning and hotfixes #780

= 3.2.3 =
* Fix: Make Search & Replace generator more robust with edge-case SQL tables #774
* Fix: _cerber_files tables are excluded and could not be copied #773

Full changelog:
[https://wp-staging.com/wp-staging-pro-changelog/](https://wp-staging.com/wp-staging-pro-changelog/)


== Upgrade Notice ==

= 4.0.4 =
* Enh: Refactor the wp_login action hook to work with different parameter count than the one in WordPress Core #1223
* Enh: Sort new staging sites in descending order by creation time #1226
* Enh: Warn if creating a backup in PHP 32 bit version #1231
* Enh: Update the backup upload success message #1221
* Enh: Show a notice if there is a new WP STAGING free version #1250
* Enh: Rename db option wpstg_existing_clones_beta to wpstg_staging_sites #1211
* Enh: Update the warning message shown when the delete process of the staging site fails #1257
* Enh: Allow use of REST API on staging sites without login #1287
* Enh: Add new EDD software licensing updater #1294
* Fix: Fix a rare issue that could happen when creating a new staging site or restoring a backup when there is a row with primary key with value zero #1271
* Fix: Try to repair MyISAM table if it's corrupt when creating a Backup #1222
* Fix: Fix an issue on backup creation that would cause a database export to loop when encountering a table with negative integers or zeros as a primary key value #1251
* Fix: Lock specific tables while exporting a backup, to prevent a rare duplicated row issue #1245
* Fix: If the memory exhausts during a database export using the Backup feature, lower the memory usage/speed of the export and try again automatically #1230
* Fix: Prevent failure of adding database to backup from causing a loop #1231
* Fix: Fix issue when old clones from version 1.1.6 or lower replaces the existing clones from later version when upgrading from FREE to PRO version #1233
* Fix: Fix inconsistent Unselect All Tables button's action #1243
* Fix: Replace undefined date with proper formatted date during backups for some warning and critical messages #1244
* Fix: Split file scanning of wp-content into scanning of plugins, themes, uploads and other directories to reduce timeout issues #1247
* Fix: Rename .user.ini to .user.ini.bak after cloning to reduce fatal errors on staging site. Also show a notice. #1255
* Fix: Skip scanning the root directory if all other directories are unselected #1256
* Fix: Show correct insufficient space message instead of permission error if unable to copy due to insufficient space #1283
* Fix: Fix showing of error when unable to count tables rows and wrap table name when fetching rows during backup #1285
* Fix: Remove custom error handler that could cause errors due to notices being thrown #1292
* Fix: Fix an error that would occur when PHP lacked permission to get the size of a directory when pushing a staging site to production #1295
* Dev: Set the version of Selenium containers to 3.141.59-20210713 to avoid issues with broken latest version of selenium #1234