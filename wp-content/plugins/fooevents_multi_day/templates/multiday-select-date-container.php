<?php
/**
 * End date options template.
 *
 * @link https://www.fooevents.com
 * @package fooevents-multiday-events
 */

?>
<div class="options_group" id ="WooCommerceEventsSelectDateContainer">
	<?php if ( ! empty( $woocommerce_events_select_date ) ) : ?>
		<?php $x = 1; ?>
		<?php foreach ( $woocommerce_events_select_date as $event_date ) : ?>
		<p class="form-field">
			<label><?php echo esc_attr( $day_term ); ?> <?php echo esc_attr( $x ); ?></label>
			<input type="text" class="WooCommerceEventsSelectDate" name="WooCommerceEventsSelectDate[]" value="<?php echo esc_attr( $event_date ); ?>"/>
		</p>
			<?php $x++; ?>
	<?php endforeach; ?>
	<?php endif; ?>
</div>
