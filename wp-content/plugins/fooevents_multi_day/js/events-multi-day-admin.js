(function($) {
    
    if (jQuery('input[name=WooCommerceEventsType]').length ) {
        
        var event_type = jQuery('input[name=WooCommerceEventsType]:checked').val();
        
        if(event_type == 'select') {
            
            var changed = false;
            fooevents_event_type_select_show(changed);
            
        }
        
        if(event_type == 'sequential') {

            fooevents_event_type_sequential_show();
            
        }
        
        jQuery('input[name=WooCommerceEventsType]').change(function(){
            
            var event_type = jQuery('input[name=WooCommerceEventsType]:checked').val();
  
            if(event_type == 'select') {
                
                var changed = true;
                fooevents_event_type_select_show(changed);

            }
            
            if(event_type == 'sequential') {

                fooevents_event_type_sequential_show();

            }
            
        });

        jQuery('#fooevents_options').on("change", "#WooCommerceEventsNumDays", function(){
            
            fooevents_display_select_date_inputs(localObjMultiDay.dayTerm);
            
        });

    }
    
    function fooevents_event_type_sequential_show() {
        
        jQuery('#WooCommerceEventsDateContainer').show();
        jQuery('#WooCommerceEventsEndDateContainer').show();
        jQuery('#WooCommerceEventsSelectDateContainer').hide();
        jQuery('#WooCommerceEventsNumDaysContainer').show();
        
    }
    
    function fooevents_event_type_select_show(changed) {
        
        jQuery('#WooCommerceEventsDateContainer').hide();
        jQuery('#WooCommerceEventsEndDateContainer').hide();
        jQuery('#WooCommerceEventsSelectDateContainer').show();
        jQuery('#WooCommerceEventsNumDaysContainer').show();
        
        if(changed) {
            
            fooevents_display_select_date_inputs(localObjMultiDay.dayTerm);
                
        }
        
    }
    
    function fooevents_display_select_date_inputs(dayTerm) {
        
        if ( jQuery('input[name="WooCommerceEventsType"]:checked').val() != 'select' ) {
            return;
        }

        jQuery('#WooCommerceEventsSelectDateContainer').show();
        
        var numDays = $('#WooCommerceEventsNumDays').val();
        
        var dateFields = '';
        for (var i = 1; i <= numDays; i++) {
            
            dateFields += '<p class="form-field">';
            dateFields += '<label>'+dayTerm+' '+i+'</label>';
            dateFields += '<input type="text" class="WooCommerceEventsSelectDate" name="WooCommerceEventsSelectDate[]" value=""/>';
            dateFields += '</p>';
            
        }
        
        jQuery('#WooCommerceEventsSelectDateContainer').html(dateFields);
        
        initSelectDatePickers();
        
    }

    function initSelectDatePickers() {

        if( (typeof localObjMultiDay === "object") && (localObjMultiDay !== null) )
        {
        
            jQuery('.WooCommerceEventsSelectDate').datepicker({
                showButtonPanel: true,
                closeText: localObjMultiDay.closeText,
                currentText: localObjMultiDay.currentText,
                monthNames: localObjMultiDay.monthNames,
                monthNamesShort: localObjMultiDay.monthNamesShort,
                dayNames: localObjMultiDay.dayNames,
                dayNamesShort: localObjMultiDay.dayNamesShort,
                dayNamesMin: localObjMultiDay.dayNamesMin,
                dateFormat: localObjMultiDay.dateFormat,
                firstDay: localObjMultiDay.firstDay,
                isRTL: localObjMultiDay.isRTL,
            });
        
        }else {

            jQuery('.WooCommerceEventsSelectDate').datepicker();

        }
        
    }
    
    initSelectDatePickers();
    
})(jQuery);