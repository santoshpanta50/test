=== Events Calendar for FooEvents ===
Contributors: fooevents, jasondugmore, robiin
Tags: calendar, events calendar, event, booking, tickets
Requires at least: 5
Tested up to: 5.9
Stable tag: 1.6.31
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Display your events in a stylish events calendar on your WordPress website. Event Calendars can be displayed using simple shortcodes or widgets. Using the free Events Calendar for FooEvents plugin, you can convert any WordPress post, page or custom post type into an event and display it in an event calendar view. This unique approach ensures that the Events Calendar for FooEvents plugin is super flexible and very easy to use.

== Description ==

The free Events Calendar for FooEvents plugin makes it possible to display your events in a stylish events calendar on your WordPress website. Calendars can be displayed using simple calendar shortcodes or calendar widgets. Using the Events Calendar for FooEvents plugin, you can convert any post, page or custom post type into an event and display it in an event calendar view. This unique approach ensures that the Events Calendar for FooEvents plugin is super flexible and very easy to use.

[Try the live demo](https://demo.fooevents.com/calendar/)

The Events Calendar for FooEvents plugin is fully compatible with the popular [FooEvents for WooCommerce](https://www.fooevents.com) plugin which makes it possible to create more advanced events and bookable services and sell branded tickets with no commission or ticket fees directly from your own WordPress website. FooEvents offers many [pro features](https://www.fooevents.com/features/) that add advanced event management functionality to your website including [free ticket themes](https://www.fooevents.com/ticket-themes/) and [check-in apps](https://www.fooevents.com/apps/).

= Events Calendar for FooEvents Features =

* Add event information to any post, page or custom post type and display these items in an event calendar
* Display multiple calendars on a single page
* Display events in calendar and list views
* Display calendars using shortcodes or widgets
* Filter events by category using shortcode parameters
* Change the look of your calendar using our built-in calendar themes ([Default](https://demo.fooevents.com/calendar/default-theme/), [Light](https://demo.fooevents.com/calendar/light-theme/), [Dark](https://demo.fooevents.com/calendar/dark-theme/), [Flat](https://demo.fooevents.com/calendar/flat-theme/), [Minimalist](https://demo.fooevents.com/calendar/minimalist-theme/)
* Automatically pull and push your events to Eventbrite!

= Add-ons =

The Events Calendar for FooEvents plugin integrates seamlessly with FooEvents for WooCommerce, making it possible to sell tickets and manage events like a pro! The [FooEvents for WooCommerce](https://www.fooevents.com) plugin enhances standard WooCommerce products and adds event and ticket selling capabilities to your website with no commission or ticket fees. To find out more about FooEvents for WooCommerce visit [FooEvents.com](https://www.fooevents.com)

* [FooEvents for WooCommerce](https://www.fooevents.com/products/fooevents-for-woocommerce/) - Sell tickets with no commission or ticket fees and manage free registration for practically any type of physical or virtual event such as a concert, class, conference, tour, webinar, fundraiser or sports fixture. FooEvents doesn't require any tech skills and you can be up and running within minutes.

* [FooEvents Bookings](https://www.fooevents.com/products/fooevents-bookings/) - Sell access to bookable events, venues, classes and services. Let your customers check availability on your website and book a space or slot. No phone calls or emails, 100% self-service. The Events Calendar for FooEvents plugin integrates seamlessly with FooEvents Bookings. Each bookable slot is displayed in the calendar and when selected, the booking is automatically selected.

* [FooEvents PDF Tickets](https://www.fooevents.com/products/fooevents-pdf-tickets/) - The FooEvents PDF Tickets plugin is an extension for FooEvents that attaches tickets as PDFs to the email that is sent to attendees or ticket purchasers instead of the default HTML ticket so that tickets can easily be saved and printed out.

* [FooEvents Custom Attendee Fields](https://www.fooevents.com/products/fooevents-custom-attendee-fields/) - Capture customized attendee fields at checkout so you can tailor FooEvents according to your unique event requirements. This is useful for acquiring additional information from attendees such as clothing sizes, meal preferences, demographic information, waiver acceptance etc. The options are virtually endless!

* [FooEvents Multi-day]( https://www.fooevents.com/products/fooevents-multi-day/) - The FooEvents Multi-day plugin is an extension for FooEvents that lets you sell tickets to events that run over multiple calendar or sequential days. This is an essential plugin if you have an event that runs longer than a day or single session and requires multiple check-ins for each attendee.

* [FooEvents Seating](https://www.fooevents.com/products/fooevents-seating/) - The FooEvents Seating plugin is an extension for FooEvents that allows your guests or attendees to select their seats at checkout based on the layout of your venue. This plugin can be used to specify rows and seats in a conference room or theater, tables and the number of chairs at the table etc. You can create VIP sections or restrict attendees to seats in other higher or lower priced areas.

* [FooEvents Express Check-in]( https://www.fooevents.com/products/fooevents-express-check-in/) - The FooEvents Express Check-in plugin is an extension for FooEvents that makes checking in and managing attendees at your event a fast and effortless process. The intuitive interface allows you to search for attendees in a matter of seconds or you can connect a Bluetooth or USB barcode scanner to scan tickets instead of typing. You also have the option of automatically checking in attendees to further speed up the process.

* [Check-ins App](https://www.fooevents.com/features/apps/) - There’s no need to struggle with clumsy spreadsheets or kill trees when you can manage event check-ins with our free, easy to use iOS and Android apps. The FooEvents Check-ins app gives you the tools to manage access to your event like a pro and it even works in offline mode and supports 15 different languages!

* [FooEvents Ticket Themes](https://www.fooevents.com/products/ticket-themes/) - Transform the appearance of your tickets and make your event stand out with our FREE Ticket Themes. If you have some basic coding knowledge, the Starter Theme can be used as a template to design your own tickets from scratch.

= Documentation and Support =

The most current documentation for the Events Calendar for FooEvents plugin can be found in the [FooEvents Help Center](https://help.fooevents.com/).

You can also purchase a [support package](https://www.fooevents.com/products/fooevents-calendar/) for the Events Calendar for FooEvents plugin should you require any technical support.

== Installation ==

= Minimum Requirements =

* PHP version 5.3 or greater (PHP 5.6 or greater is recommended)
* MySQL version 5.0 or greater (MySQL 5.6 or greater is recommended)
* WooCommerce 3.1.0 

= Automatic installation =

Automatic installation is the easiest option as WordPress handles all the file transfers. To do an automatic installation of the Events Calendar for FooEvents plugin, login to your WordPress dashboard, navigate to the Plugins menu and click 'Add New'.

In the search field type "FooEvents" and click 'Search Plugins'. Once you've found the Events Calendar for FooEvents plugin, you can view details about it such as release information, rating and description. Most importantly of course, you can install it by simply clicking "Install Now".

= Manual installation =

The manual installation method involves downloading the Events Calendar for FooEvents plugin and uploading it to your web server via your favorite FTP application. The WordPress codex contains [instructions on how to do this here](https://codex.wordpress.org/Managing_Plugins#Manual_Plugin_Installation).

= Updating =

Automatic updates should work seamlessly, but as always though, please ensure that you back-up your website just in case.

= Events Calendar for FooEvents Settings =

* Navigate to your WordPress admin area
* Navigate to Settings > Events Calendar for FooEvents
* Various settings can be configured that will change the behavior and display of your calendar

= Getting Started =

* Navigate to the post or page where you would like to insert the calendar on your website
* Type or paste the calendar shortcode in the main content text editor
* Examples of calendar shortcodes include:
 * Basic calendar - **[fooevents_calendar]**
 * Display particular month - **[fooevents_calendar defaultDate= "2019-09-01"]**
 * Display list view - **[fooevents_calendar defaultView="listWeek"]**
 * Display multiple calendars per page - **[fooevents_calendar id="cal1"]**, **[fooevents_calendar id="cal2"]** etc.
* To add a widget view of the calendar:
 * Go to Appearance -> Widgets
 * Drag the Events Calendar for FooEvents widget into the widget area where you would like it to be displayed
 * Type a widget title
 * Select which type of view you would like to use - Calendar or List view
 * Calendar view:
  * You can specify which date you would like the calendar to start on. If left empty, the calendar will default to the current date.
  * Example date format: 2016-09-01 (yyyy/mm/dd)
 * List view:
  * You can set the number of events to be displayed. By default, the list view will display the latest 5 events.
 * Click 'Save' to confirm your changes 	
* Other shortcodes
 * Simple event - **[fooevents_event product=10]**
 * Simple list of events - **[fooevents_events_list]**
 * List of 5 events - **[fooevents_events_list num=5]**


== Frequently Asked Questions ==

= I need technical support. How can I get help? =

The Events Calendar for FooEvents is free to use and the latest documentation can be found in the [FooEvents Help Center](https://help.fooevents.com/). If you require any further assistance, please purchase a [support package](https://www.fooevents.com/products/fooevents-calendar/) through the FooEvents website and submit a [support ticket]( https://help.fooevents.com/contact/support/) using your order number so that we can validate your support license. 

= I’m interested in purchasing a license for the FooEvents for WooCommerce plugins but I have a few pre-sale questions. Can somebody help me? =

Absolutely! For any pre-sales questions that you might have, we suggest first going through the [FAQ section](https://help.fooevents.com/docs/frequently-asked-questions/) on our website which contains many of the common questions that we get asked. If your query isn't listed there, please feel free to [contact us](https://help.fooevents.com/contact/).

= I have a feature request. What's the best way to get in touch? =

The features and direction of our products is guided entirely by the feedback that we receive from the FooEvents community. If you would like to suggest a new feature for future consideration, please [contact us](https://help.fooevents.com/contact/).

= Do I need the FooEvents for WooCommerce plugin to create events and use the Events Calendar for FooEvents? =

No. You can create events from standard posts, pages and custom posts types in WordPress. When you activate the Events Calendar for FooEvents plugin, an additional meta box will be added to posts and pages making it possible to add event information to these post types. The [FooEvents for WooCommerce](https://www.fooevents.com/products/fooevents-for-woocommerce/) plugin is only required if you would like to sell physical or virtual tickets to your events.

= How do I set a post type to include event information? =

When the Events Calendar for FooEvents plugin is activated, all posts and pages will include an extra event meta box that can be used to add event information. You can configure additional post types to include event information by following these steps:
* Navigate to your WordPress admin area
* Navigate to Settings -> FooEvents Calendar
* Set the post types that should be associated with events by selecting the desired post types from the ‘Associate with post types’ multi-select box
* Click “Save Changes”

= How do I change the calendar design? =

The Events Calendar for FooEvents includes several themes which change the look and feel of the calendar to suit your preferences. Here are the steps to change the calendar theme:
* Navigate to your WordPress admin area
* Navigate to Settings -> FooEvents Calendar
* Select a calendar theme from the calendar theme drop-down menu
* Click “Save Changes”

= Do you offer discounts for Nonprofit Organizations (NPO)? =

We believe in the ‘pay it forward’ approach to business and life. As such, we offer registered Nonprofit Organizations (NPO) a discount on any FooEvents product. To apply for this discount please complete the [NPO application form](https://help.fooevents.com/contact/npo-discount-application/) and if successful, you will receive a discount coupon that you can use at checkout within a couple of days.

== Screenshots ==

1. Add a product to the cart by selecting if from the list or by scanning its barcode
2. Checkout to complete the order
3. View your sales history, cancel completed orders and print receipts
4. View a summary of the day's sales
5. Manage product prices and stock quantities

== Changelog ==
= 1.6.31 =
* FIXED: PHP 8.0 compatibility *
* FIXED: Duplicate booking options when using calendar shortcode bug *
* FIXED: Various other small bugs *
* TESTED ON: WordPress 5.9 and WooCommerce 6.2.0 *

= 1.6.29 =
* FIXED: Various small bugs *
* TESTED ON: WordPress 5.8.2 and WooCommerce 5.9.0 *

= 1.6.28 =
* FIXED: Various small bugs *
* TESTED ON: WordPress 5.8.1 and WooCommerce 5.6.0 *

= 1.6.26 =
* FIXED: Book ticket link on event listings *
* TESTED ON: WordPress 5.8 and WooCommerce 5.6.0 *

= 1.6.25 =
* FIXED: Eventbrite import/export bug *
* FIXED: Event displaying single and bookings dates bug *
* FIXED: Support for D F j date format *
* FIXED: Various translation import bugs *
* FIXED: Various other small bugs *
* TESTED ON: WordPress 5.8 and WooCommerce 5.6.0 *

= 1.6.19 =
* FIXED: Settings page formatting when FooEvents for WooCommerce is not active *
* FIXED: Various small bugs *
* TESTED ON: WordPress 5.7.2 and WooCommerce 5.3.0 *

= 1.6.17 =
* FIXED: End date event list bug *
* FIXED: Book ticket '1' label bug *
* FIXED: Various small bugs *
* TESTED ON: WordPress 5.7.2 and WooCommerce 5.3.0 *

= 1.6.15 =
* FIXED: Added non-product check to processing *
* FIXED: PHP version number warnings in script methods *
* ADDED: Global start date option now has settings for Calendar, Event List, Both or Disabled *
* FIXED: Various small bugs *
* TESTED ON: WordPress 5.7 and WooCommerce 5.1.0 *

= 1.6.11 =
* FIXED: Bug where select dates not working on non-product event *
* TESTED ON: WordPress 5.7 and WooCommerce 5.1.0 *

= 1.6.10 =
* UPDATED: Various security and standards compliance updates *
* FIXED: Various small bugs *
* TESTED ON: WordPress 5.7 and WooCommerce 5.1.0 *

= 1.6.8 =
* FIXED: Various small bugs *
* TESTED ON: WordPress 5.6.1 and WooCommerce 5.0.0 *

= 1.6.3 =
* FIXED: Bookings on events calendar list *
* FIXED: Various small calendar bugs *
* TESTED Events Calendar for FooEvents tested ON: WordPress 5.6 and WooCommerce 4.8.0 *

= 1.6.0 =
* FIXED: Eventbrite calendar import bug *
* FIXED: Multi-day calendar display bug *
* FIXED: Various small calendar bugs *
* TESTED Events Calendar for FooEvents tested ON: WordPress 5.5.3 and WooCommerce 4.6.1 

= 1.5.26 =
* ADDED: Calendar Event expiration support *

= 1.5.25 =
* FIXED: Bug where non-product calendar events cannot be disabled *
* FIXED: Eventbrite API bug retrieve users calendar events *
* FIXED: End date format bug on ListWeek view *
* FIXED: Wrong event hour on some servers *
* FIXED: Various small calendar bugs *

= 1.5.19 =
* FIXED: French calendar month name translations *
* FIXED: Various small calendar bugs *

= 1.5.17 =
* FIXED: Various small calendar bugs *

= 1.5.16 =
* FIXED: Eventbrite API calendar bugs *
* FIXED: Various other small calendar bugs *

= 1.5.14 =
* FIXED: Various small calendar bugs *

= 1.5.10 =
* FIXED: Various small calendar bugs

= 1.5.9 =
* ADDED: Support for calendar event timezone setting
* FIXED: Various small calendar bugs

= 1.5.0 =
* NEW: Support for new calendar event reporting features
* FIXED: Various small calendar bugs

= 1.4.21 =
* FIXED: Multi-day display calendar bug 
* FIXED: Event grouping to today in calendar bug
* FIXED: Various other small calendar bugs

= 1.4.16 =
* FIXED: Eventbrite calendar import updates

= 1.4.14 =
* FIXED: Various small calendar bugs

= 1.4.12 =
* FIXED: Various small calendar bugs

= 1.4.9 =
* ADDED: Don't display book ticket button on event list if out of stock 
* FIXED: Date format event not displaying on calendar bug 
* FIXED: Various other small calendar bugs

= 1.4.6 =
* ADDED: Eventbrite calendar import support
* FIXED: Various bug calendar fixes

= 1.4.0 =
* ADDED: Calendar event options to other post types
* ADDED: Calendar and event list selectable themes
* ADDED: Eventbrite calendar import support
* FIXED: Various small calendar bug fixes

= 1.3.14 =

* FIXED: Multi-day calendar format bug 
* UPDATED: FullCalendar library
* ADDED: Option to display 24 hour time format on calendar 
* ADDED: Option to only display event start date on multi-day events within the calendar

= 1.3.10 =

* UPDATED: Calendar Documentation
* UPDATED: Plugin option clean-up on delete 
* FIXED: Various small calendar bugs

= 1.3.9 =

* FIXED: Terminology new site bug
* FIXED: Various other small calendar bugs

= 1.3.8 =

* ADDED: Calendar terminology override feature

= 1.3.7 =

* FIXED: Update notification service bug
* FIXED: Calendar date format bug

= 1.3.5 =

* FIXED: Calendar AM PM bug
* FIXED: Various small calendar bugs

= 1.3.3 =

* ADDED: Support for colored event days on calendar
* FIXED: Various small calendar bugs

= 1.2.8 =

* FIXED: Event list description bug 
* ADDED: Option to enable full-day calendar events 

= 1.2.6 =

* FIXED: Apostrophe bug
* FIXED: Widget limit bug
* ADDED: Sort event list 
* ADDED: Category Filter

= 1.2.1 =

* FIXED: Bug where events with no date showed on calendar
* FIXED: Calendar translations bug
* UPDATED: FullCalendar

= 1.1.2 =

* FIXED: Multi-day support calendar update

= 1.1.0 =

* ADDED: Multi-day extension ready

= 1.0.13 =

* FIXED: PHP warnings

= 1.0.11 =

* FIXED: Calendar description bug
* FIXED: Special characters in calendar event titles
* FIXED: Various other minor calendar bugs
* ADDED: calendar Translations

= 1.0.6 =

* FIXED: PHP warning output

= 1.0.5 =

* ADDED: Additional user roles now have access
* ADDED: Plugin update notification
* FIXED: Various minor bugs

== Screenshots ==

1. Default Theme
2. Light Theme
3. Flat Theme
4. Minimalist Theme
5. Dark Theme
6. List View