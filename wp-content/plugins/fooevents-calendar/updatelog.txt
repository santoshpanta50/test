
UPDATE LOG

------------------
1.6.31
FIXED: PHP 8.0 compatibility
FIXED: Duplicate booking options when using calendar shortcode bug
FIXED: Various other small bugs
TESTED ON: WordPress 5.9 and WooCommerce 6.2.0

1.6.29
FIXED: Various small bugs
TESTED ON: WordPress 5.8.2 and WooCommerce 5.9.0

1.6.28
FIXED: Various small bugs
TESTED ON: WordPress 5.8.1 and WooCommerce 5.6.0

1.6.26
FIXED: Book ticket link on event listings
TESTED ON: WordPress 5.8 and WooCommerce 5.6.0

1.6.25
FIXED: Eventbrite import/export bug
FIXED: Event displaying single and bookings dates bug
FIXED: Support for D F j date format
FIXED: Various translation import bugs
FIXED: Various other small bugs
TESTED ON: WordPress 5.8 and WooCommerce 5.6.0

1.6.19
FIXED: Settings page formatting when FooEvents for WooCommerce is not active
FIXED: Various small bugs
TESTED ON: WordPress 5.7.2 and WooCommerce 5.3.0

1.6.17
FIXED: End date event list bug 
FIXED: Book ticket '1' label bug
FIXED: Various small bugs
TESTED ON: WordPress 5.7.2 and WooCommerce 5.3.0

1.6.15
FIXED: Added non-product check to processing
FIXED: PHP version number warnings in script methods
ADDED: Global start date option now has settings for Calendar, Event List, Both or Disabled 
FIXED: Various small bugs
TESTED ON: WordPress 5.7 and WooCommerce 5.1.0

1.6.11
FIXED: Bug where select dates not working on non-product event
TESTED ON: WordPress 5.7 and WooCommerce 5.1.0

1.6.10
UPDATED: Various security and standards compliance updates 
FIXED: Various small bugs
TESTED ON: WordPress 5.7 and WooCommerce 5.1.0

1.6.8
FIXED: Various small bugs
TESTED ON: WordPress 5.6.1 and WooCommerce 5.0.0

1.6.7
FIXED: SESSION warning
FIXED: listWeek end time bug 
FIXED: Various other small bugs
TESTED ON: WordPress 5.6 and WooCommerce 4.9.1

1.6.3
FIXED: Bookings on events list
FIXED: Various small bugs
TESTED ON: WordPress 5.6 and WooCommerce 4.8.0

1.6.0
FIXED: Eventbrite import bug
FIXED: Multi-day display bug
FIXED: Various small bugs
TESTED ON: WordPress 5.5.3 and WooCommerce 4.6.1

1.5.26
ADDED: Event expiration support
TESTED ON: WordPress 5.5.1 and WooCommerce 4.5.2

1.5.25
FIXED: Bug where non-product events cannot be disabled
FIXED: Eventbrite API bug retrieve users events
FIXED: End date format bug on ListWeek view
FIXED: Wrong event hour on some servers
FIXED: Various small bugs
TESTED ON: WordPress 5.5.1 and WooCommerce 4.5.2

1.5.19
FIXED: French month name translations
FIXED: Various small bugs
TESTED ON: WordPress 5.5 and WooCommerce 4.3.2

1.5.17
FIXED: Various small bugs
TESTED ON: WordPress 5.4.2 and WooCommerce 4.2.2

1.5.16
FIXED: Eventbrite API bugs
FIXED: Various other small bugs
TESTED ON: WordPress 5.4.2 and WooCommerce 4.2.2

1.5.14
FIXED: Various small bugs
TESTED ON: WordPress 5.4.1 and WooCommerce 4.2.0

1.5.11
FIXED: Various small bugs
TESTED ON: WordPress 5.4 and WooCommerce 4.0.1

1.5.10
FIXED: Various small bugs
TESTED ON: WordPress 5.4 and WooCommerce 4.0.1

1.5.9
ADDED: Support for event timezone setting
FIXED: Various small bugs

1.5.6
FIXED: Wrong settings link on plugins page
FIXED: Eventbrite array association bug
FIXED: Fatal error when WooCommerce is not active
FIXED: Various other small bugs

1.5.3
FIXED: French month bug

1.5.1
FIXED: JavaScript tooltip conflict

1.5.0
NEW: Support for new reporting features
FIXED: Various small bugs

1.4.21
FIXED: Multi-day display bug 
FIXED: Event grouping to today bug
FIXED: Various other small bugs