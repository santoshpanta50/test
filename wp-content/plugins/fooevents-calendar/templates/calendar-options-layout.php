<?php
/**
 * Template for FooEvents Calendar options layout
 *
 * @file    FooEvents Calendar global options layout
 * @link    https://www.fooevents.com
 * @since   1.0.0
 * @package fooevents-calendar
 */

?>
<div class="wrap" id="fooevents-calendar-options-page">
	<form method="post" action="options.php">
		<table class="form-table">
			<?php echo $calendar_options; ?>
		</table>
		<?php submit_button(); ?>
	</form>
</div>
