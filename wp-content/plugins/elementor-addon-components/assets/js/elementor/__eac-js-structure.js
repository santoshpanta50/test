
/**
 * Description: Cette méthode est déclenchée lorsque le composant 'xxxxxxxxx' est chargé dans la page
 *
 * @param {selector} $scope. Le contenu de la section
 * @since 1.8.9
 */
;(function($, elementor) {

	'use strict';

	var EacAddonsXXXXXX = {

		init: function() {
			elementor.hooks.addAction('frontend/element_ready/xxxxxxxxx.default', EacAddonsXXXXXX.widgetXXXXXX);
		},
		
		widgetXXXXXX: function widgetXXXXXX($scope) {
			
		},
	};
	
	
	/**
	* Description: Cette méthode est déclenchée lorsque le frontend Elementor est initialisé
	*
	* @return (object) Initialise l'objet EacAddonsXXXXXX
	* @since 0.0.9
	*/
	$(window).on('elementor/frontend/init', EacAddonsXXXXXX.init);
	
}(jQuery, window.elementorFrontend));