
/**
 * Object get_CurrentPosition
 *
 * @return {object[]} Tableau d'objets Latitude, Longitude du client au format JSON
 * @since 1.8.8
 */
var get_CurrentPosition = {
	lat: null,
	lng: null,
	accuracy: null,
	
	init: function() {
		// Le host n'est pas HTTPS et 127.0.0.1 ou ne supporte pas la geo localisation
		if((window.location.protocol !== 'https:' && window.location.host !== '127.0.0.1') || !navigator.geolocation) {
			return false;
		}
		// récupère la position du client
		var options = { enableHighAccuracy: true, timeout: 5000, maximumAge: 0 };
		navigator.geolocation.getCurrentPosition(get_CurrentPosition.set_LatLng);
	},
	
	print_Error: function(error) {
		
	},
	
	set_LatLng: function(response) {
		get_CurrentPosition.lat = response.coords.latitude;
		get_CurrentPosition.lng = response.coords.longitude;
		get_CurrentPosition.accuracy = response.coords.accuracy;
		//console.log("Lat-Lng : " + get_CurrentPosition.lat + "::" + get_CurrentPosition.lng, get_CurrentPosition.accuracy);
	},
		
	get_LatLng: function() {
		return { "lat": get_CurrentPosition.lat, "lng": get_CurrentPosition.lng }
	},
};

window.addEventListener("load", function(event) {
	//get_CurrentPosition.init();
});

/**
 * Description: Cette méthode est déclenchée lorsque la section 'eac-addon-open-streetmap' est chargée dans la page
 *
 * @param {selector} $scope. Le contenu de la section
 * @since 1.8.8
 */
jQuery(window).on("elementor/frontend/init", function() {
	elementorFrontend.hooks.addAction("frontend/element_ready/eac-addon-open-streetmap.default", function($scope, $) {
		var $targetInstance = $scope.find('.eac-open-streetmap'),
			$targetWrapper = $targetInstance.find('.osm-map_wrapper'),
			$targetMarkerCenter = $targetWrapper.find('.osm-map_wrapper-markercenter'),
			$targetMarkers = $targetWrapper.find('.osm-map_wrapper-marker'),
			settings = $targetWrapper.data('settings') || {},
			scriptUrl = eacOSM.pluginsUrl,
			osmMap,
			mapData = {
				mapDiv: null,
				lat: null,
				lng: null,
				title: '',
				content: '',
			},
			markerArray = [],
			osm_basic = L.tileLayer("http://{s}.tile.osm.org/{z}/{x}/{y}.png", {
				attribution: "© <a href='http://osm.org/copyright'>OpenStreetMap</a> contributors",
				label: 'OSM Basic',
			}),
			osm_fr = L.tileLayer("http://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png" , {
				attribution: "© <a href=\"http://osm.org/copyright\">OpenStreetMap</a> contributors",
				label: "OSM France",
			}),
			osm_de = L.tileLayer("https://{s}.tile.openstreetmap.de/tiles/osmde/{z}/{x}/{y}.png" , {
				attribution: "© <a href=\"http://osm.org/copyright\">OpenStreetMap</a> contributors",
				label: "OSM Deutschland",
			}),
			osm_bw = L.tileLayer("http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png", {
				attribution: "© <a href='http://osm.org/copyright'>OpenStreetMap</a> contributors",
				label: 'OSM B&W',
			}),
			stamenToner = L.tileLayer('//{s}.tile.stamen.com/toner/{z}/{x}/{y}.png', {
				attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> — Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
				subdomains: 'abcd',
				maxZoom: 20,
				minZoom: 0,
				label: 'Toner'
			}),
			stamenColor = L.tileLayer('//{s}.tile.stamen.com/watercolor/{z}/{x}/{y}.png', {
				attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> — Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
				subdomains: 'abcd',
				maxZoom: 16,
				minZoom: 1,
				label: 'Watercolor'
			}),
			stamenLite = L.tileLayer('//{s}.tile.stamen.com/toner-lite/{z}/{x}/{y}.png', {
				attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> — Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
				subdomains: 'abcd',
				maxZoom: 20,
				minZoom: 0,
				label: 'Toner Lite'
			}),
			stamenTerrain = L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/terrain/{z}/{x}/{y}.png', {
				attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> — Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
				subdomains: 'abcd',
				maxZoom: 18,
				minZoom: 0,
				label: 'Terrain'
			}),
			topoMap = L.tileLayer('//a.tile.opentopomap.org/{z}/{x}/{y}.png', {
				attribution: 'Map tiles by <a href="http://opentopomap.org">Open Topomap</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> — Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors',
				subdomains: 'abcd',
				maxZoom: 17,
				minZoom: 0,
				label: 'Topo Map'
			});
			
		
		// Erreur settings
		if(Object.keys(settings).length === 0) {
			return;
		}
		  
		// Affichage de la carte, des controls et des marqueurs
		var show_OsmMap = function() {
			
			mapData.mapDiv = settings.data_id;
			mapData.tile = $targetMarkerCenter.find('.osm-map_marker-title')[0].innerText;
			mapData.content = $targetMarkerCenter.find('.osm-map_marker-content')[0].innerHTML;
			mapData.lat = $targetMarkerCenter.data('lat');
			mapData.lng = $targetMarkerCenter.data('lng');
			
			//console.log([JSON.parse(JSON.stringify(osm_fr)).options.label]);
			// Le control layer et les entrées
			var baseLayers = {
				"OSM Basic": osm_basic,
				"OSM France": osm_fr,
				"OSM Deutschland": osm_de,
				"OSM B&W": osm_bw,
				"Toner": stamenToner,
				"Watercolor": stamenColor,
				"Toner Lite": stamenLite,
				'Terrain': stamenTerrain,
				"Topo Map": topoMap,
			};
			
			// Construit une nouvelle icone
			var newIcon = L.Icon.extend({
				options: {
					shadowUrl: scriptUrl + '/assets/images/marker-shadow.png',
					iconSize:     [33, 44],
					iconAnchor:   [17, 44],
					shadowSize:   [41, 41],
					shadowAnchor: [17, 41],
					popupAnchor:  [0, -37]
				}
			});
			
			// Création de la carte
			var map = L.map(settings.data_id, {
				center: [mapData.lat, mapData.lng],
				layers: [eval(settings.data_layer)],
				closePopupOnClick: settings.data_clickpopup,
				zoom: settings.data_zoom,
				zoomControl: !settings.data_zoompos,
			});
			
			// Positionne le control zoom
			if(settings.data_zoompos) {
				L.control.zoom({position: 'bottomleft'}).addTo(map);
			}
			
			// Ajout du control des layers
			L.control.layers(baseLayers).addTo(map);
			
			// Ajout du marker central à la map et affichage de la popup
			L.marker([mapData.lat, mapData.lng]).addTo(map).bindPopup('<span>' + mapData.tile + '</span><br>' + mapData.content).openPopup();
			
			// Ajout du marqueur à la liste des marqueurs
			markerArray.push(L.marker(new L.LatLng(mapData.lat, mapData.lng)));
			
			// Boucle sur les marqueurs
			$.each($targetMarkers, function(index, marker) {
				var lat = $(marker).data('lat');
				var lng = $(marker).data('lng');
				var icon = $(marker).data('icon');
				var title = $(marker).find('.osm-map_marker-title')[0].innerText;
				var content = $(marker).find('.osm-map_marker-content')[0].innerHTML;
				
				// Affecte le chemin de la nouvelle icone
				var customIcon = new newIcon({ iconUrl: scriptUrl + '/assets/images/osm-icons/' + icon });
				
				// Ajout du marker à la map
				L.marker([lat, lng], {icon: customIcon}).addTo(map).bindPopup('<span>' + title + '</span><br>' + content);
				
				// Ajout du marqueur à la liste des marqueurs
				markerArray.push(L.marker(new L.LatLng(lat, lng)));
			});
			
			// Zoom automatique
			if(markerArray.length > 0 && settings.data_zoomauto) {
				var notContained = [];
				
				// Boucle sur tous les marqueurs
				map.eachLayer(function(theLayer) {
					// Le marqueur n'est pas dans les limites visibles de la map
					if(theLayer instanceof L.Marker && !map.getBounds().contains(theLayer.getLatLng())) {
						notContained.push(theLayer);
					}
				});
				
				// Applique le zoom qui englobe tous les marqueurs
				if(notContained.length > 0) {
					var group = L.featureGroup(markerArray);
					map.fitBounds(group.getBounds(), { padding: [50, 50] });
				}
			}
			
			// Supprime le zoom roulette de la souris
			settings.data_wheelzoom === false ? map.scrollWheelZoom.disable() : '';
			
			// Supprime le zoom double click
			settings.data_dblclick === false ? map.doubleClickZoom.disable() : '';
			
			// Supprime le fond de carte draggable
			settings.data_draggable === false ? map.dragging.disable() : '';
			
			return map;
		};
		
		// Timeout pour laisser le temps de retrouver la position du client. Désactivé
		if(settings.data_geolocate) {
			setTimeout(function() {
				var pos = get_CurrentPosition.get_LatLng();
				if(pos.lat !== null) {
					mapData.lat = pos.lat;
					mapData.lng = pos.lng;
					osmMap = show_OsmMap();
					//console.log("LatLng : " + mapData.lat + "::" + mapData.lng);
				} else {
					osmMap = show_OsmMap();
				}
			}, 3000);
		} else {
			osmMap = show_OsmMap();
		}
			
		
	});
})
