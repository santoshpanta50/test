<?php

/*============================================================================================================================
* Class: Open_Streetmap_Widget
* Name: OpenStreetMap
* Slug: eac-addon-open-streetmap
*
* Icon by: https://templatic.com/newsblog/100-free-templatic-map-icons/
*
* Description: Affiche une Map et ses marqueurs avec le projet OpenStreetMap alternatif à GoogleMap
* Projet collaboratif de cartographie en ligne qui vise à constituer une base de données géographiques libre du monde
*
*
* @since 1.8.8
*============================================================================================================================*/

namespace EACCustomWidgets\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Core\Schemes\Typography;
use Elementor\Core\Schemes\Color;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Repeater;
use Elementor\Group_Control_Css_Filter;

if(! defined('ABSPATH')) exit; // Exit if accessed directly

class Open_Streetmap_Widget extends Widget_Base {
	
    /*
    * Retrieve widget name.
    *
    * @access public
    *
    * @return widget name.
    */
    public function get_name() {
        return 'eac-addon-open-streetmap';
    }

    /*
    * Retrieve widget title.
    *
    * @access public
    *
    * @return widget title.
    */
    public function get_title() {
        return __("OpenStreetMap", 'eac-components');
    }

    /*
    * Retrieve widget icon.
    *
    * @access public
    *
    * @return widget icon.
	* https://char-map.herokuapp.com/
    */
    public function get_icon() {
        return 'eicon-google-maps eac-icon-elements';
    }
	
	/* 
	* Affecte le composant à la catégorie définie dans plugin.php
	* 
	* @access public
    *
    * @return widget category.
	*/
	public function get_categories() {
		return ['eac-advanced'];
	}
	
	/* 
	* Load dependent libraries
	* 
	* @access public
    *
    * @return libraries list.
	*/
	public function get_script_depends() {
		return ['eac-leaflet', 'eac-openstreetmap'];
	}
	
	/** 
	 * Load dependent styles
	 * Les styles sont chargés dans le footer
	 * 
	 * @access public
	 *
	 * @return CSS list.
	 */
	public function get_style_depends() {
		return ['eac-leaflet'];
	}
	
	/**
	 * Get widget keywords.
	 *
	 * Retrieve the list of keywords the widget belongs to.
	 *
	 * @since 1.7.0
	 * @access public
	 *
	 * @return array Widget keywords.
	 */
	public function get_keywords() {
		return ['map', 'openstreetmap'];
	}
	
	/**
	 * Get help widget get_custom_help_url.
	 *
	 * 
	 *
	 * @since 1.7.0
	 * @access public
	 *
	 * @return URL help center
	 */
	public function get_custom_help_url() {
        return 'https://elementor-addon-components.com/how-to-add-openstreetmap-to-elementor/';
    }
	
	private $custom_icons = array(
		'default.png' => 'Default',
		'automotive.png' => 'Automotive',
		'bars.png' => 'Bars',
		'books-media.png' => 'Books & Media',
		'clothings.png' => 'Clothings',
		'commercial-places.png' => 'Commercial places',
		'doctors.png' => 'Doctors',
		'exhibitions.png' => 'Exhibitions',
		'fashion.png' => 'Fashion',
		'food.png' => 'Food',
		'government.png' => 'Government',
		'health-medical.png' => 'Health Medical',
		'hotels.png' => 'Hotels',
		'industries.png' => 'Industries',
		'libraries.png' => 'Libraries',
		'magazines.png' => 'Magazines',
		'movies.png' => 'Movies',
		'museums.png' => 'Museums',
		'nightlife.png' => 'Nightlife',
		'parks.png' => 'Parks',
		'places.png' => 'Places',
		'real-estate.png' => 'Real Estate',
		'restaurants.png' => 'Restaurants',
		'schools.png' => 'Schools',
		'sports.png' => 'Sports',
		'swimming-pools.png' => 'Swimming-pools',
		'transport.png' => 'Transport',
		'travel.png' => 'Travel',
	);
	
	private $base_layers = array(
		'osm_basic' => 'OSM Basic',
		'osm_fr' => 'OSM France',
		'osm_de' => 'OSM Deutschland',
		'osm_bw' => 'OSM B&W',
		'stamenToner' => 'Toner',
		'stamenColor' => 'Watercolor',
		'stamenLite' => 'Toner Lite',
		'stamenTerrain' => 'Terrain',
		'topoMap' => 'Topo Map',
	);
	
    /*
    * Register widget controls.
    *
    * Adds different input fields to allow the user to change and customize the widget settings.
    *
    * @access protected
    */
	protected function register_controls() {
		
		$this->start_controls_section('osm_settings_map',
			[
				'label'	=> __('Carte', 'eac-components'),
				'tab'	=> Controls_Manager::TAB_CONTENT,
			]
		);
			
			/*$this->add_control('osm_settings_client_geolocate',
				[
					'label' => __("Localiser le visiteur (Géolocaliser)", 'eac-components'),
					'type' => Controls_Manager::SWITCHER,
					'description' => __('La gélocalisation doit être activée', 'eac-components'),
					'label_on' => __('oui', 'eac-components'),
					'label_off' => __('non', 'eac-components'),
					'return_value' => 'yes',
					'default' => '',
					'conditions' => [
						'terms' => [
							['name' => 'osm_settings_client_ip', 'operator' => '!==', 'value' => 'yes'],
							['name' => 'osm_settings_search', 'operator' => '!==', 'value' => 'yes'],
						],
					],
				]
			);*/
			
			$this->add_control('osm_settings_client_ip',
				[
					'label' => __("Localiser le visiteur (Adresse IP)", 'eac-components'),
					'type' => Controls_Manager::SWITCHER,
					'description' => __("Localisation par l'adresse IP.<br>Ne fonctionne pas sur un serveur local.", 'eac-components'),
					'label_on' => __('oui', 'eac-components'),
					'label_off' => __('non', 'eac-components'),
					'return_value' => 'yes',
					'default' => '',
					'conditions' => [
						'terms' => [
							//['name' => 'osm_settings_client_geolocate', 'operator' => '!==', 'value' => 'yes'],
							['name' => 'osm_settings_search', 'operator' => '!==', 'value' => 'yes'],
						],
					],
				]
			);
			
			$this->add_control('osm_settings_client_warning',
				[
					'type' => Controls_Manager::RAW_HTML,
					'content_classes' => 'elementor-panel-alert elementor-panel-alert-warning',
					'raw'  => __("La géolocalisation vers la bonne ville peut être moins fiable pour les adresses IP distribuées par les opérateurs mobiles.", "eac-components"),
					'condition' => ['osm_settings_client_ip' => 'yes'],
				]
			);
			
			$this->add_control('osm_settings_search',
				[
					'label' => __("Rechercher une adresse", 'eac-components'),
					'type' => Controls_Manager::SWITCHER,
					'label_on' => __('oui', 'eac-components'),
					'label_off' => __('non', 'eac-components'),
					'return_value' => 'yes',
					'default' => '',
					'conditions' => [
						'terms' => [
							//['name' => 'osm_settings_client_geolocate', 'operator' => '!==', 'value' => 'yes'],
							['name' => 'osm_settings_client_ip', 'operator' => '!==', 'value' => 'yes'],
						],
					],
				]
			);
			
			$this->add_control('osm_settings_search_help',
				[
					'label'       => __('', 'eac-components'),
					'type'        => Controls_Manager::RAW_HTML,
					'raw'         => __("<span style='font-size:10px;'>Entrer l'adresse puis bouton 'Search'</span>", 'eac-components'),
					'condition' => ['osm_settings_search' => 'yes'],
				]
			);
			
			$this->add_control('osm_settings_search_addresse', // elementor-control-osm_settings_search_addresse
				[
					'label'       => __('Adresse', 'eac-components'),
					'type'        => Controls_Manager::RAW_HTML,
					'raw'         => '<form onsubmit="getNominatimAddress(this);" action="javascript:void(0);"><input type="text" id="eac-get-nominatim-address" class="eac-get-nominatim-address" style="margin-top:10px; margin-bottom:10px;"><input type="submit" value="Search" class="elementor-button elementor-button-default" onclick="getNominatimAddress(this)"></form>',
					'label_block' => true,
					'condition' => ['osm_settings_search' => 'yes'],
				]
			);
			
			$this->add_control('osm_settings_center_lat', // elementor-control-osm_settings_center_lat
				[
					'label' => __("Latitude", 'eac-components'),
					'type' => Controls_Manager::TEXT,
					'dynamic' => ['active' => true],
					'label_block' => true,
					'condition' => ['osm_settings_search' => 'yes'],
				]
			);
			
			$this->add_control('osm_settings_center_lng', // elementor-control-osm_settings_center_lng
				[
					'label' => __("Longitude", 'eac-components'),
					'type' => Controls_Manager::TEXT,
					'dynamic' => ['active' => true],
					'label_block' => true,
					'condition' => ['osm_settings_search' => 'yes'],
				]
			);
			
			$this->add_control('osm_settings_center_help',
				[
					'label'       => __('', 'eac-components'),
					'type'        => Controls_Manager::RAW_HTML,
					'raw'         => __('<span style="font-size:10px;">Cliquez <a href="https://www.coordonnees-gps.fr/" target="_blank" rel="nofollow" >ici</a> pour obtenir des coordonnées de localisation</span>', 'eac-components'),
					'condition' => ['osm_settings_search' => 'yes'],
				]
			);
			
			$this->add_control('osm_settings_center_title', // elementor-control-osm_settings_center_title
				[
					'label' => __("Titre de l'infobulle", 'eac-components'),
					'type' => Controls_Manager::TEXT,
					//'default' => __("Titre de l'infobulle", 'eac-components'),
					'placeholder' => __("Titre de l'infobulle", 'eac-components'),
					'dynamic' => ['active' => true],
					'label_block' => true,
					'separator' => 'before',
				]
			);
			
			$this->add_control('osm_settings_center_content',
				[
					'label' => __("Contenu de l'infobulle", 'eac-components'),
					'type' => Controls_Manager::TEXTAREA,
					'placeholder' => __("Contenu de l'infobulle", 'eac-components'),
					'dynamic' => ['active' => true],
					'label_block' => true,
				]
			);
			
		$this->end_controls_section();
		
		$this->start_controls_section('osm_markers',
			[
				'label'	=> __('Marqueurs', 'eac-components'),
				'tab'	=> Controls_Manager::TAB_CONTENT,
			]
		);
			
			$repeater = new Repeater();
			
			$repeater->start_controls_tabs('osm_markers_tabs');
				
				$repeater->start_controls_tab('osm_markers_tab_position',
					[
						'label' => '<i class="awesome-position" aria-hidden="true"></i>',
					]
				);
					
					$repeater->add_control('osm_markers_search_help',
						[
							'label' => __('', 'eac-components'),
							'type' => Controls_Manager::RAW_HTML,
							'raw' => __("<span style='font-size:10px;'>Entrer l'adresse puis bouton 'Search'</span>", 'eac-components'),
						]
					);
					
					$repeater->add_control('osm_markers_search_addresse', // elementor-control-osm_markers_search_addresse
						[
							'label' => __('Adresse', 'eac-components'),
							'type' => Controls_Manager::RAW_HTML,
							'raw' => '<form onsubmit="getNominatimRepeaterAddress(this);" action="javascript:void(0);"><input type="text" id="eac-get-nominatim-address" class="eac-get-nominatim-address" style="margin-top:10px; margin-bottom:10px;"><input type="submit" value="Search" class="elementor-button elementor-button-default" onclick="getNominatimRepeaterAddress(this)"></form>',
							'label_block' => true,
						]
					);
					
					$repeater->add_control('osm_markers_tooltip_lat', // elementor-control-osm_markers_tooltip_lat
						[
							'label' => __("Latitude", 'eac-components'),
							'type' => Controls_Manager::TEXT,
							'dynamic' => ['active' => true],
							'label_block' => true,
						]
					);
					
					$repeater->add_control('osm_markers_tooltip_lng', // elementor-control-osm_markers_tooltip_lng
						[
							'label' => __("Longitude", 'eac-components'),
							'type' => Controls_Manager::TEXT,
							'dynamic' => ['active' => true],
							'label_block' => true,
						]
					);
					
					$repeater->add_control('osm_markers_tooltip_help',
						[
							'label'       => __('', 'eac-components'),
							'type'        => Controls_Manager::RAW_HTML,
							'raw'         => __('<span style="font-size:10px;">Cliquez <a href="https://www.coordonnees-gps.fr/" target="_blank" rel="nofollow" >ici</a> pour obtenir des coordonnées de localisation</span>', 'eac-components'),
						]
					);
					
				$repeater->end_controls_tab();
				
				$repeater->start_controls_tab('osm_markers_tab_content',
					[
						'label' => '<i class="awesome-content" aria-hidden="true"></i>',
					]
				);
					
					$repeater->add_control('osm_markers_tooltip_title', // elementor-control-osm_markers_tooltip_title
						[
							'label' => __("Titre de l'infobulle", 'eac-components'),
							'type' => Controls_Manager::TEXT,
							'placeholder' => __("Titre de l'infobulle", 'eac-components'),
							'dynamic' => ['active' => true],
							'label_block' => true,
						]
					);
					
					$repeater->add_control('osm_markers_tooltip_content',
						[
							'label' => __("Contenu de l'infobulle", 'eac-components'),
							'type' => Controls_Manager::TEXTAREA,
							'placeholder' => __("Contenu de l'infobulle", 'eac-components'),
							'dynamic' => ['active' => true],
							'label_block' => true,
							//'render_type' => 'none',
						]
					);
					
					$repeater->add_control('osm_markers_tooltip_marker',
						[
							'label' => __('Sélectionner une icône', 'eac-components'),
							'type' => Controls_Manager::SELECT,
							'options' => $this->custom_icons,
							'default' => 'default.png',
							'label_block' => true,
						]
					);
				
				$repeater->end_controls_tab();
			
			$repeater->end_controls_tabs();
			
			$this->add_control('osm_markers_list',
				[
					'label'       => __('Liste des marqueurs', 'eac-components'),
					'type'        => Controls_Manager::REPEATER,
					'fields'      => $repeater->get_controls(),
					'title_field' => '{{{ osm_markers_tooltip_title }}}',
				]
			);
			
		$this->end_controls_section();
		
		$this->start_controls_section('osm_settings',
			[
				'label'	=> __('Réglages', 'eac-components'),
				'tab'	=> Controls_Manager::TAB_CONTENT,
			]
		);
			
			$this->add_control('osm_settings_zoom_auto',
				[
					'label' => __("Zoom automatique", 'eac-components'),
					'type' => Controls_Manager::SWITCHER,
					'description' => __("Afficher tous les marqueurs dans le viewport.", 'eac-components'),
					'label_on' => __('oui', 'eac-components'),
					'label_off' => __('non', 'eac-components'),
					'return_value' => 'yes',
					'default' => '',
				]
			);
			
			$this->add_control('osm_settings_zoom',
				[
					'label' => __('Facteur de zoom', 'eac-components'),
					'type'  => Controls_Manager::NUMBER,
					'min' => 1,
					'max' => 20,
					'default' => 12,
					'step' => 1,
					'condition' => ['osm_settings_zoom_auto!' => 'yes'],
				]
			);
			
			$this->add_responsive_control('osm_settings_height',
				[
					'label' => __("Hauteur min.", 'eac-components'),
					'type'  => Controls_Manager::SLIDER,
					'size_units' => ['px'],
					'default' => ['unit' => 'px', 'size' => 350],
					'range' => ['px' => ['min' => 150, 'max' => 1000, 'step' => 50]],
					'selectors' => ['{{WRAPPER}} .osm-map_wrapper-map' => 'min-height: {{SIZE}}{{UNIT}};'],
					'render_type' => 'template',
				]
			);
			
			$this->add_control('osm_settings_layers',
				[
					'label' => __('Sélectionner le calque par défaut', 'eac-components'),
					'type' => Controls_Manager::SELECT,
					'options' => $this->base_layers,
					'default' => 'osm_basic',
					'label_block' => true,
				]
			);
			
		$this->end_controls_section();
		
		$this->start_controls_section('osm_content',
			[
				'label'	=> __('Controls', 'eac-components'),
				'tab'	=> Controls_Manager::TAB_CONTENT,
			]
		);
			
			$this->add_control('osm_content_zoom_position',
				[
					'label' => __("Zoom en bas à gauche", 'eac-components'),
					'type' => Controls_Manager::SWITCHER,
					'label_on' => __('oui', 'eac-components'),
					'label_off' => __('non', 'eac-components'),
					'return_value' => 'yes',
					'default' => '',
				]
			);
			
			$this->add_control('osm_content_zoom',
				[
					'label' => __("Zoomer avec la souris", 'eac-components'),
					'type' => Controls_Manager::SWITCHER,
					'label_on' => __('oui', 'eac-components'),
					'label_off' => __('non', 'eac-components'),
					'return_value' => 'yes',
					'default' => 'yes',
				]
			);
			
			$this->add_control('osm_content_dblclick',
				[
					'label' => __("Double click pour zoomer", 'eac-components'),
					'type' => Controls_Manager::SWITCHER,
					'label_on' => __('oui', 'eac-components'),
					'label_off' => __('non', 'eac-components'),
					'return_value' => 'yes',
					'default' => 'yes',
				]
			);
			
			$this->add_control('osm_content_draggable',
				[
					'label' => __("Faire glisser la carte", 'eac-components'),
					'type' => Controls_Manager::SWITCHER,
					'label_on' => __('oui', 'eac-components'),
					'label_off' => __('non', 'eac-components'),
					'return_value' => 'yes',
					'default' => 'yes',
				]
			);
			
			$this->add_control('osm_content_click_popup',
				[
					'label' => __("Clicker pour fermer les info-bulles", 'eac-components'),
					'type' => Controls_Manager::SWITCHER,
					'label_on' => __('oui', 'eac-components'),
					'label_off' => __('non', 'eac-components'),
					'return_value' => 'yes',
					'default' => 'yes',
				]
			);
			
		$this->end_controls_section();
		
		/**
		 * Generale Style Section
		 */
		$this->start_controls_section('osm_global_style',
			[
				'label'      => __('Carte', 'eac-components'),
				'tab'        => Controls_Manager::TAB_STYLE,
			]
		);
			
			$this->add_group_control(
			Group_Control_Border::get_type(),
				[
					'name' => 'osm_global_border',
					'selector' => '{{WRAPPER}} .osm-map_wrapper-map',
				]
			);
			
			$this->add_control('osm_global_border_radius',
				[
					'label' => __('Rayon de la bordure', 'eac-components'),
					'type' => Controls_Manager::DIMENSIONS,
					'size_units' => ['px', '%'],
					'selectors' => ['{{WRAPPER}} .osm-map_wrapper-map' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'],
				]
			);
			
			$this->add_group_control(
    			Group_Control_Box_Shadow::get_type(),
    			[
    				'name' => 'osm_global_border_shadow',
    				'label' => __('Ombre', 'eac-components'),
    				'selector' => '{{WRAPPER}} .osm-map_wrapper-map',
    			]
    		);
		    
			$this->add_group_control(
			Group_Control_Css_Filter::get_type(),
				[
					'name' => 'css_filters',
					'selector' => '{{WRAPPER}} .osm-map_wrapper-map',
				]
			);
			
		$this->end_controls_section();
		
		$this->start_controls_section('osm_title_style',
			[
				'label'      => __("Titre de l'infobulle", 'eac-components'),
				'tab'        => Controls_Manager::TAB_STYLE,
			]
		);
			
			$this->add_control('osm_title_color',
				[
					'label' => __('Couleur', 'eac-components'),
					'type' => Controls_Manager::COLOR,
					'scheme' => [
						'type' => Color::get_type(),
						'value' => Color::COLOR_4,
					],
					'default' => '#000000',
					'selectors' => ['{{WRAPPER}} .leaflet-popup-content span' => 'color: {{VALUE}};']
				]
			);
					
			$this->add_group_control(
			Group_Control_Typography::get_type(),
				[
					'name' => 'osm_title_typography',
					'label' => __('Typographie', 'eac-components'),
					'scheme' => Typography::TYPOGRAPHY_1,
					'selector' => '{{WRAPPER}} .leaflet-popup-content span',
				]
			);
					
			$this->add_responsive_control('osm_title_position',
				[
					'label' => __('Alignement', 'eac-components'),
					'type' => Controls_Manager::CHOOSE,
					'default' => 'center',
					'options' => [
						'left' => [
							'title' => __('Gauche', 'eac-components'),
							'icon' => 'eicon-text-align-left',
						],
						'center' => [
							'title' => __('Centre', 'eac-components'),
							'icon' => 'eicon-text-align-center',
						],
						'right' => [
							'title' => __('Droit', 'eac-components'),
							'icon' => 'eicon-text-align-right',
						],
					],
					'selectors' => ['{{WRAPPER}} .leaflet-popup-content span' => 'text-align: {{VALUE}};']
				]
			);
			
		$this->end_controls_section();
    }
	
	
	/*
	* Render widget output on the frontend.
	*
	* Written in PHP and used to generate the final HTML.
	*
	* @access protected
	*/
    protected function render() {
		?>
		<div class="eac-open-streetmap">
			<?php $this->render_map(); ?>
		</div>
		<?php
    }
	
	protected function render_map() {
		$settings = $this->get_settings_for_display();
		$id = $this->get_id();
		
		// Les balises acceptées pour le contenu du tooltip
		$allowed_content = array('br' => array(), 'p' => array(), 'strong' => array(), 'a' => array('href' => array(), 'target' => array(), 'rel' => array()));
		
		// Les valeurs par défaut: Paris
		$center_lat = 48.8579;
		$center_lng = 2.3491;
		$center_title = !empty($settings['osm_settings_center_title']) ? wp_kses_post($settings['osm_settings_center_title']) : __("Titre de l'infobulle", 'eac-components');
		$center_content = !empty($settings['osm_settings_center_content']) ? wp_kses($settings['osm_settings_center_content'], $allowed_content) : '';
		
		// La liste des marqueurs
		$map_markers = $settings['osm_markers_list'];
		
		// Coordonnées à partir de l'adresse IP
		$client_ip = $settings['osm_settings_client_ip'] === 'yes' ? true : false;
		
		// Le moteur de recherche
		$client_search = $settings['osm_settings_search'] === 'yes' ? true : false;
		
		if($client_search && !empty($settings['osm_settings_center_lat']) && !empty($settings['osm_settings_center_lng'])) {
				$center_lat = !empty($settings['osm_settings_center_lat']) ? $settings['osm_settings_center_lat'] : $center_lat;
				$center_lng = !empty($settings['osm_settings_center_lng']) ? $settings['osm_settings_center_lng'] : $center_lng;
		
		 // Calcule de l'adresse IP du client
		} else if($client_ip && isset($_SERVER['REMOTE_ADDR']) && !empty($_SERVER['REMOTE_ADDR'])) {
			$ip_address = sanitize_text_field(wp_unslash($_SERVER['REMOTE_ADDR']));
			//$ip = unserialize(file_get_contents('http://ip-api.com/json/'. $ip_address));
			$ip = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=$ip_address"));
			if($ip['geoplugin_status'] === 200) {
				$center_lat = isset($ip['geoplugin_latitude']) ? $ip['geoplugin_latitude'] : $center_lat;
				$center_lng = isset($ip['geoplugin_longitude']) ? $ip['geoplugin_longitude'] : $center_lng;
				$center_title = isset($ip['geoplugin_city']) ? wp_kses_post($ip['geoplugin_city']) : $center_title;
				if(isset($ip['geoplugin_countryName'])) { $center_title .= ", " . wp_kses_post($ip['geoplugin_countryName']); }
			}
			//console_log($ip);
		}
		
		// La div wrapper
		$this->add_render_attribute('osm_wrapper', 'class', 'osm-map_wrapper');
		$this->add_render_attribute('osm_wrapper', 'data-settings', $this->get_settings_json($id));
		
		// La div du marqueur central
		$this->add_render_attribute('osm_marker', 'class', 'osm-map_wrapper-markercenter');
		$this->add_render_attribute('osm_marker', 'data-lat', $center_lat);
		$this->add_render_attribute('osm_marker', 'data-lng', $center_lng);
		?>
		<div <?php echo $this->get_render_attribute_string('osm_wrapper'); ?>>
			<!-- La div de la carte -->
			<div id="<?php echo $id; ?>" class="osm-map_wrapper-map"></div>
			
			<!-- Le marqueur central -->
			<div <?php echo $this->get_render_attribute_string('osm_marker'); ?>>
				<div class="osm-map_marker-title"><?php echo $center_title; ?></div>
				<div class="osm-map_marker-content"><?php echo $center_content; ?></div>
			</div>
			<?php
			/** Les marqueurs du repeater */
			foreach($map_markers as $index => $marker) {
				if(!empty($marker['osm_markers_tooltip_lat']) && !empty($marker['osm_markers_tooltip_lng'])) {
					$key = 'osm_markers_' . $index;
					$this->add_render_attribute(
						$key,
						array(
							"class" => "osm-map_wrapper-marker",
							"data-lat" => $marker['osm_markers_tooltip_lat'],
							"data-lng" => $marker['osm_markers_tooltip_lng'],
							"data-icon" => $marker['osm_markers_tooltip_marker'],
						)
					);
					?>
					<div <?php echo $this->get_render_attribute_string($key); ?>>
						<div class="osm-map_marker-title"><?php echo wp_kses_post($marker['osm_markers_tooltip_title']); ?></div>
						<div class="osm-map_marker-content"><?php echo wp_kses($marker['osm_markers_tooltip_content'], $allowed_content); ?></div>
					</div>
				<?php
				}
			}
			?>
		</div>
		<?php
	}
	
	/*
	* get_settings_json
	*
	* Retrieve fields values to pass at the widget container
    * Convert on JSON format
    * Read by 'openstreetmap.js' file when the component is loaded on the frontend
	*
	* @uses      json_encode()
	*
	* @return    JSON oject
	*
	* @access    protected
	* @updated   1.8.8
	*/
	protected function get_settings_json($id) {
		$module_settings = $this->get_settings_for_display();
		$locate = false; //$module_settings['osm_settings_client_geolocate'] === 'yes' ? true : false;
		$zoomauto = $module_settings['osm_settings_zoom_auto'] === 'yes' ? true : false;
		
		$settings = array(
			"data_id" => $id,
			"data_geolocate" => $locate,
			"data_zoom" => $zoomauto ? 12 : $module_settings['osm_settings_zoom'],
			"data_zoompos" => $module_settings['osm_content_zoom_position'] === 'yes' ? true : false,
			"data_zoomauto" => $zoomauto,
			"data_layer" => $module_settings['osm_settings_layers'],
			"data_wheelzoom" => $module_settings['osm_content_zoom'] === 'yes' ? true : false,
			"data_dblclick" => $module_settings['osm_content_dblclick'] === 'yes' ? true : false,
			"data_draggable" => $module_settings['osm_content_draggable'] === 'yes' ? true : false,
			"data_clickpopup" => $module_settings['osm_content_click_popup'] === 'yes' ? true : false,
		);
		
		$settings = json_encode($settings);
		return $settings;
	}
	
	protected function content_template() {}
	
}