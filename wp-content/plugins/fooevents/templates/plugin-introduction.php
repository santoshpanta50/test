<?php
/**
 * Ticket themes viewer template
 *
 * @link https://www.fooevents.com
 * @package woocommerce_events
 */

$config = new FooEvents_Config();
if ( ! function_exists( 'is_plugin_active' ) || ! function_exists( 'is_plugin_active_for_network' ) ) {

	require_once ABSPATH . '/wp-admin/includes/plugin.php';

}
?>
<div class='woocommerce-events-help'>
	<h1><?php esc_attr_e('Welcome to FooEvents for WooCommerce', 'woocommerce-events'); ?></h1>

	<p> 
        <a href="https://help.fooevents.com/" target="_blank"><?php esc_attr_e('Documentation', 'woocommerce-events'); ?></a> | 
        <a href="https://help.fooevents.com/docs/frequently-asked-questions/" target="_blank"><?php esc_attr_e('Frequently Asked Questions', 'woocommerce-events'); ?></a> 
    </p>

	<h3 class="woocommerce-events-intro"><?php esc_attr_e('Thank you for purchasing FooEvents for WooCommerce!', 'woocommerce-events'); ?></h3>  

    <?php esc_attr_e('FooEvents works great out of the box without any custom configuration, however, if you would like to configure FooEvents based on your specific event requirements, you can find out more about how to do this by visiting our', 'woocommerce-events'); ?> <a href="https://help.fooevents.com/" target="_blank"><?php esc_attr_e('help documentation', 'woocommerce-events'); ?></a>.

    <div class="clear"></div> 

	<div class="woocommerce-events-infobox">
        <h3><?php esc_attr_e('Next Steps', 'woocommerce-events'); ?></h3>
        <ol>
            <li><a href="https://help.fooevents.com/docs/fooevents-for-woocommerce/setup/global-settings/" target="_blank"><?php esc_attr_e('Configure the FooEvents global settings (optional)', 'woocommerce-events'); ?></a></li> 
            <li><strong><a href="https://help.fooevents.com/docs/fooevents-for-woocommerce/setup/setup-an-event-product/" target="_blank"><?php esc_attr_e('Setup your first event', 'woocommerce-events'); ?></a></strong></li> 
            <li><a href="https://www.fooevents.com/ticket-themes/" target="_blank"><?php esc_attr_e('Customize your email tickets using Ticket Themes', 'woocommerce-events'); ?></a></li>
            <li><a href="https://www.fooevents.com/apps/" target="_blank"><?php esc_attr_e('Install the free FooEvents Check-ins Apps', 'woocommerce-events'); ?></a></li> 
            <li><a href="https://www.fooevents.com/foosales-integration/" target="_blank"><?php esc_attr_e('Sell tickets at your event with FooSales', 'woocommerce-events'); ?></a></li>
        </ol>
    </div>

	<div class="woocommerce-events-infobox">
        <h3><?php esc_attr_e('Helpful Resources', 'woocommerce-events'); ?></h3>
        <ul>
            <li><a href="https://www.fooevents.com/2018/03/07/create-different-ticket-types-fooevents-using-woocommerce-variations-attributes/" target="_blank"><?php esc_attr_e('How to create different ticket types in FooEvents using WooCommerce variations and attributes', 'woocommerce-events'); ?></a></li>
            <li><a href="https://www.fooevents.com/2018/02/28/how-to-create-reoccurring-events-using-fooevents-multi-day-plugin/" target="_blank"><?php esc_attr_e('How to create reoccurring events using FooEvents Multi-day plugin', 'woocommerce-events'); ?></a></li>
            <li><a href="https://www.fooevents.com/2018/04/05/get-creative-with-fooevents-ticket-themes/" target="_blank"><?php esc_attr_e('Get creative with FooEvents Ticket Themes', 'woocommerce-events'); ?></a></li>
            <li><a href="https://www.fooevents.com/speed-up-your-woocommerce-website/" target="_blank"><?php esc_attr_e('Speed up your WooCommerce Website', 'woocommerce-events'); ?></a></li>
            <li><a href="http://demo.fooevents.com/" target="_blank"><?php esc_attr_e('FooEvents Demo', 'woocommerce-events'); ?></a>
        </ul> 
    </div>

	<div class="clear"></div> 

	<h3><?php esc_attr_e('FooEvents Extensions', 'woocommerce-events'); ?></h3>

    <p><?php esc_attr_e('The following extensions add various advanced features to the FooEvents for WooCommerce plugin. They can be purchased separately or as part of our popular', 'woocommerce-events'); ?> <a href="https://www.fooevents.com/pricing/" target="_blank"><?php esc_attr_e('bundles', 'woocommerce-events'); ?></a>. <?php esc_attr_e('If you would like to upgrade to a bundle, please', 'woocommerce-events'); ?> <a href="https://www.fooevents.com/support/" target="_blank"><?php esc_attr_e('contact us', 'woocommerce-events'); ?></a> <?php esc_attr_e('and we will gladly assist', 'woocommerce-events'); ?>.</p>

	<div class="woocommerce-events-extensions">

		<?php
		if ( is_plugin_active( 'fooevents_custom_attendee_fields/fooevents-custom-attendee-fields.php' ) || is_plugin_active_for_network( 'fooevents_custom_attendee_fields/fooevents-custom-attendee-fields.php' ) ) {
			$installed = true;
		} else {
			$installed = false; }
		?>
		<div class="woocommerce-events-extension 
		<?php
		if ( false === $installed ) {
			echo 'not-installed'; }
		?>
		">    

			<a href="https://www.fooevents.com/product/fooevents-custom-attendee-fields/" target="_blank"><img src="https://www.fooevents.com/wp-content/uploads/2017/07/fooevents_product_covers_custom_attendee_fields-512x512.png" alt="<?php esc_attr_e('FooEvents Custom Attendee Fields', 'woocommerce-events'); ?>" /></a>
			<h3><a href="https://www.fooevents.com/product/fooevents-custom-attendee-fields/" target="_blank"><?php esc_attr_e('FooEvents Custom Attendee Fields', 'woocommerce-events'); ?></a></h3>
            <p><?php esc_attr_e('Capture customized attendee fields at checkout and tailor FooEvents according to your unique event requirements.', 'woocommerce-events'); ?></p>
            <strong><?php esc_attr_e('Status:', 'woocommerce-events'); ?></strong>  

			<?php
			if (is_plugin_active('fooevents_custom_attendee_fields/fooevents-custom-attendee-fields.php') || is_plugin_active_for_network('fooevents_custom_attendee_fields/fooevents-custom-attendee-fields.php')) {
                echo "<span class='install-status installed'>" . esc_attr('Installed', 'woocommerce-events') . "</span>| <a href='https://www.fooevents.com/fooevents-custom-attendee-fields/' target='new'>" . esc_attr('Plugin Details', 'woocommerce-events') . "</a>"; 
            } else {
                if( file_exists(ABSPATH . 'wp-content/plugins/fooevents_custom_attendee_fields/fooevents-custom-attendee-fields.php')) { 
                    echo "<span class='install-status notinstalled'>" . esc_attr('Deactivated', 'woocommerce-events') . "</span>"; 
                } else { 
                    echo "<span class='install-status notinstalled'>" . esc_attr('Not Installed', 'woocommerce-events') . "</span>| <a href='https://www.fooevents.com/fooevents-custom-attendee-fields/' target='new'>" . esc_attr('Get Plugin', 'woocommerce-events') . "</a>"; 
                }  
            } 
			?>
			| <a href="https://help.fooevents.com/docs/fooevents-custom-attendee-fields/"><?php esc_attr_e('Documentation', 'woocommerce-events'); ?></a> 
			<div class="clear"></div>   
		</div>
		<?php
		if ( is_plugin_active( 'fooevents_seating/fooevents-seating.php' ) || is_plugin_active_for_network( 'fooevents_seating/fooevents-seating.php' ) ) {
			$installed = true;
		} else {
			$installed = false; }
		?>
		<div class="woocommerce-events-extension 
		<?php
		if ( false === $installed ) {
			echo 'not-installed'; }
		?>
		">    
			<a href="https://www.fooevents.com/fooevents-seating/" target="_blank"><img src="https://www.fooevents.com/wp-content/uploads/2018/08/fooevents-seating-1-150x150.png" alt="<?php esc_attr_e('FooEvents Seating', 'woocommerce-events'); ?>" /></a>
            <h3><a href="https://www.fooevents.com/fooevents-seating/" target="_blank"><?php esc_attr_e('FooEvents Seating', 'woocommerce-events'); ?></a></h3>
            <p><?php esc_attr_e('The FooEvents Seating plugin is an extension for FooEvents that allows your guests or attendees to select their seats at checkout based on the layout of your venue.', 'woocommerce-events'); ?></p>
            <strong><?php esc_attr_e('Status:', 'woocommerce-events'); ?></strong> 
			<?php
			if ( is_plugin_active( 'fooevents_seating/fooevents-seating.php' ) || is_plugin_active_for_network( 'fooevents_seating/fooevents-seating.php' ) ) {
				echo "<span class='install-status installed'>" . esc_attr('Installed', 'woocommerce-events') . "</span>| <a href='https://www.fooevents.com/fooevents-seating/' target='new'>" . esc_attr('Plugin Details', 'woocommerce-events') . "</a>";
			} else {
				if ( file_exists( ABSPATH . 'wp-content/plugins/fooevents_seating/fooevents-seating.php' ) ) {
					echo "<span class='install-status notinstalled'>" . esc_attr('Deactivated', 'woocommerce-events') . "</span>";
				} else {
					echo "<span class='install-status notinstalled'>" . esc_attr('Not Installed', 'woocommerce-events') . "</span>| <a href='https://www.fooevents.com/fooevents-seating/' target='new'>" . esc_attr('Get Plugin', 'woocommerce-events') . "</a>";
				}
			}
			?>
			| <a href="https://help.fooevents.com/docs/fooevents-seating/"><?php esc_attr_e('Documentation', 'woocommerce-events'); ?></a> 
			<div class="clear"></div>        
		</div>
		<?php
		if ( is_plugin_active( 'fooevents_multi_day/fooevents-multi-day.php' ) || is_plugin_active_for_network( 'fooevents_multi_day/fooevents-multi-day.php' ) ) {
			$installed = true;
		} else {
			$installed = false; }
		?>
		<div class="woocommerce-events-extension 
		<?php
		if ( false === $installed ) {
			echo 'not-installed'; }
		?>
		">    
			<a href="https://www.fooevents.com/fooevents-multi-day/" target="_blank"><img src="https://www.fooevents.com/wp-content/uploads/2017/07/fooevents_product_covers_multiday-512x512.png" alt="<?php esc_attr_e('FooEvents Multi-day', 'woocommerce-events'); ?>" /></a>
			<h3><a href="https://www.fooevents.com/fooevents-multi-day/" target="_blank"><?php esc_attr_e('FooEvents Multi-day', 'woocommerce-events'); ?></a></h3>
			<p><?php esc_attr_e('The FooEvents Multi-day plugin adds support for events that run over multiple days such as concerts, conferences and exhibitions.', 'woocommerce-events'); ?></p>
			<strong><?php esc_attr_e('Status:', 'woocommerce-events'); ?></strong> 
			<?php
			if ( is_plugin_active( 'fooevents_multi_day/fooevents-multi-day.php' ) || is_plugin_active_for_network( 'fooevents_multi_day/fooevents-multi-day.php' ) ) {
				echo "<span class='install-status installed'>" . esc_attr('Installed', 'woocommerce-events') . "</span>| <a href='https://www.fooevents.com/fooevents-multi-day/' target='new'>" . esc_attr('Plugin Details', 'woocommerce-events') . "</a>";
			} else {
				if ( file_exists( ABSPATH . 'wp-content/plugins/fooevents_multi_day/fooevents-multi-day.php' ) ) {
					echo "<span class='install-status notinstalled'>" . esc_attr('Deactivated', 'woocommerce-events') . "</span>";
				} else {
					echo "<span class='install-status notinstalled'>" . esc_attr('Not Installed', 'woocommerce-events') . "</span>| <a href='https://www.fooevents.com/fooevents-multi-day/' target='new'>" . esc_attr('Get Plugin', 'woocommerce-events') . "</a>";
				}
			}
			?>
			| <a href="https://help.fooevents.com/docs/fooevents-multi-day/"><?php esc_attr_e('Documentation', 'woocommerce-events'); ?></a> 
			<div class="clear"></div>        
		</div>
		<?php
		if ( is_plugin_active( 'fooevents_pdf_tickets/fooevents-pdf-tickets.php' ) || is_plugin_active_for_network( 'fooevents_pdf_tickets/fooevents-pdf-tickets.php' ) ) {
			$installed = true;
		} else {
			$installed = false; }
		?>
		<div class="woocommerce-events-extension 
		<?php
		if ( false === $installed ) {
			echo 'not-installed'; }
		?>
		">
			<a href="https://www.fooevents.com/product/fooevents-pdf-tickets/" target="_blank"><img src="https://www.fooevents.com/wp-content/uploads/2017/07/fooevents_product_covers_pdf-tickets-512x512.png" alt="<?php esc_attr_e('FooEvents PDF Tickets Plugin', 'woocommerce-events'); ?>" /></a>
			<h3><a href="https://www.fooevents.com/product/fooevents-pdf-tickets/" target="_blank"><?php esc_attr_e('FooEvents PDF Tickets Plugin', 'woocommerce-events'); ?></a></h3>
			<p><?php esc_attr_e('The FooEvents PDF Tickets plugin attaches event tickets as PDF files to the email that is sent to the attendee or ticket purchaser.', 'woocommerce-events'); ?></p>
			<strong><?php esc_attr_e('Status:', 'woocommerce-events'); ?></strong> 
			<?php
			if ( is_plugin_active( 'fooevents_pdf_tickets/fooevents-pdf-tickets.php' ) || is_plugin_active_for_network( 'fooevents_pdf_tickets/fooevents-pdf-tickets.php' ) ) {
				echo "<span class='install-status installed'>" . esc_attr('Installed', 'woocommerce-events') . "</span> | <a href='https://www.fooevents.com/fooevents-pdf-tickets/' target='new'>" . esc_attr('Plugin Details', 'woocommerce-events') . "</a>";
			} else {
				if ( file_exists( ABSPATH . 'wp-content/plugins/fooevents_pdf_tickets/fooevents-pdf-tickets.php' ) ) {
					echo "<span class='install-status notinstalled'>" . esc_attr('Deactivated', 'woocommerce-events') . "</span>";
				} else {
					echo "<span class='install-status notinstalled'>" . esc_attr('Not Installed', 'woocommerce-events') . "</span>| <a href='https://www.fooevents.com/fooevents-pdf-tickets/' target='new'>" . esc_attr('Get Plugin', 'woocommerce-events') . "</a>";
				}
			}
			?>
			| <a href="https://help.fooevents.com/docs/fooevents-pdf-tickets/"><?php esc_attr_e('Documentation', 'woocommerce-events'); ?></a> 
			<div class="clear"></div>   

		</div>

		<?php
		if ( is_plugin_active( 'fooevents-calendar/fooevents-calendar.php' ) || is_plugin_active_for_network( 'fooevents-calendar/fooevents-calendar.php' ) ) {
			$installed = true;
		} else {
			$installed = false; }
		?>
		<div class="woocommerce-events-extension 
		<?php
		if ( false === $installed ) {
			echo 'not-installed'; }
		?>
		">
			<a href="https://www.fooevents.com/product/fooevents-calendar/" target="_blank"><img src="https://www.fooevents.com/wp-content/uploads/2017/07/fooevents_product_covers_calendar-512x512.png" alt="<?php esc_attr_e('FooEvents Calendar', 'woocommerce-events'); ?>" /></a>
			<h3><a href="https://www.fooevents.com/product/fooevents-calendar/" target="_blank"><?php esc_attr_e('FooEvents Calendar', 'woocommerce-events'); ?></a></h3>
			<p><?php esc_attr_e('The FooEvents Calendar plugin makes it possible to display event lists and calendars using shortcodes and widgets.', 'woocommerce-events'); ?></p>
			<strong><?php esc_attr_e('Status:', 'woocommerce-events'); ?></strong> 
			<?php
			if ( is_plugin_active( 'fooevents-calendar/fooevents-calendar.php' ) || is_plugin_active_for_network( 'fooevents-calendar/fooevents-calendar.php' ) ) {
				echo "<span class='install-status installed'>" . esc_attr('Installed', 'woocommerce-events') . "</span> | <a href='https://www.fooevents.com/fooevents-calendar/' target='new'>" . esc_attr('Plugin Details', 'woocommerce-events') . "</a>";
			} else {
				if ( file_exists( ABSPATH . 'wp-content/plugins/fooevents-calendar/fooevents-calendar.php' ) ) {
					echo "<span class='install-status notinstalled'>" . esc_attr('Deactivated', 'woocommerce-events') . "</span>";
				} else {
					echo "<span class='install-status notinstalled'>" . esc_attr('Not Installed', 'woocommerce-events') . "</span> | <a href='https://www.fooevents.com/fooevents-calendar/' target='new'>" . esc_attr('Get Plugin', 'woocommerce-events') . "</a>";
				}
			}
			?>
			| <a href="https://help.fooevents.com/docs/fooevents-calendar/"><?php esc_attr_e('Documentation', 'woocommerce-events'); ?></a> 
			<div class="clear"></div> 

		</div>

		<?php
		if ( is_plugin_active( 'fooevents_express_check_in/fooevents-express-check_in.php' ) || is_plugin_active_for_network( 'fooevents_express_check_in/fooevents-express-check_in.php' ) ) {
			$installed = true;
		} else {
			$installed = false; }
		?>
		<div class="woocommerce-events-extension 
		<?php
		if ( false === $installed ) {
			echo 'not-installed'; }
		?>
		">
			<a href="https://www.fooevents.com/product/fooevents-express-check-in/" target="_blank"><img src="https://www.fooevents.com/wp-content/uploads/2017/07/fooevents_product_covers_express_checkins-512x512.png" alt="<?php esc_attr_e('FooEvents Express Check-in', 'woocommerce-events'); ?>" /></a>
			<h3><a href="https://www.fooevents.com/product/fooevents-express-check-in/" target="_blank"><?php esc_attr_e('FooEvents Express Check-in', 'woocommerce-events'); ?></a></h3>
			<p><?php esc_attr_e('The FooEvents Express Check-in plugin ensures that checking in attendees at your event is fast and effortless.', 'woocommerce-events'); ?></p>
			<strong><?php esc_attr_e('Status:', 'woocommerce-events'); ?></strong> 
			<?php
			if ( is_plugin_active( 'fooevents_express_check_in/fooevents-express-check_in.php' ) || is_plugin_active_for_network( 'fooevents_express_check_in/fooevents-express-check_in.php' ) ) {
				echo "<span class='install-status installed'>" . esc_attr('Installed', 'woocommerce-events') . "</span> | <a href='https://www.fooevents.com/fooevents-express-check-in/' target='new'>" . esc_attr('Plugin Details', 'woocommerce-events') . "</a>";
			} else {
				if ( file_exists( ABSPATH . 'wp-content/plugins/fooevents_express_check_in/fooevents-express-check_in.php' ) ) {
					echo "<span class='install-status notinstalled'>" . esc_attr('Deactivated', 'woocommerce-events') . "</span>";
				} else {
					echo "<span class='install-status notinstalled'>" . esc_attr('Not Installed', 'woocommerce-events') . "</span> | <a href='https://www.fooevents.com/fooevents-express-check-in/' target='new'>" . esc_attr('Get Plugin', 'woocommerce-events') . "</a>";
				}
			}
			?>
			| <a href="https://help.fooevents.com/docs/fooevents-express-check-in/"><?php esc_attr_e('Documentation', 'woocommerce-events'); ?></a> 
			<div class="clear"></div>  

		</div>
		<div class="clear"></div>
	</div>
</div>
