<?php
/**
 * Global options
 *
 * @link https://www.fooevents.com
 * @package woocommerce_events
 */

?>
<div class="wrap" id="fooevents-settings-page">
	<h1 class="wp-heading-inline"><?php esc_attr_e( 'FooEvents Settings', 'woocommerce-events' ); ?></h1>
	<h2 class="nav-tab-wrapper">
		<a href="?page=fooevents-settings&tab=api" class="nav-tab <?php echo 'api' === $active_tab ? 'nav-tab-active' : ''; ?>"><?php esc_attr_e( 'License', 'woocommerce-events' ); ?></a>
		<a href="?page=fooevents-settings&tab=general" class="nav-tab <?php echo 'general' === $active_tab ? 'nav-tab-active' : ''; ?>"><?php esc_attr_e( 'General', 'woocommerce-events' ); ?></a>
		<a href="?page=fooevents-settings&tab=terminology" class="nav-tab <?php echo 'terminology' === $active_tab ? 'nav-tab-active' : ''; ?>"><?php esc_attr_e( 'Terminology', 'woocommerce-events' ); ?></a>
		<a href="?page=fooevents-settings&tab=ticket_design" class="nav-tab <?php echo 'ticket_design' === $active_tab ? 'nav-tab-active' : ''; ?>"><?php esc_attr_e( 'Ticket Design', 'woocommerce-events' ); ?></a>
		<?php if ( $pdf_enabled ) : ?>
			<a href="?page=fooevents-settings&tab=pdf" class="nav-tab <?php echo 'pdf' === $active_tab ? 'nav-tab-active' : ''; ?>"><?php esc_attr_e( 'PDF Tickets', 'woocommerce-events' ); ?></a>
		<?php endif; ?>
		<?php if ( $seating_enabled ) : ?>
			<a href="?page=fooevents-settings&tab=seating" class="nav-tab <?php echo 'seating' === $active_tab ? 'nav-tab-active' : ''; ?>"><?php esc_attr_e( 'Seating', 'woocommerce-events' ); ?></a>
		<?php endif; ?>
		<?php if ( $calendar_enabled ) : ?>
			<a href="?page=fooevents-settings&tab=calendar" class="nav-tab <?php echo 'calendar' === $active_tab ? 'nav-tab-active' : ''; ?>"><?php esc_attr_e( 'Calendar', 'woocommerce-events' ); ?></a>
		<?php endif; ?>
		<a href="?page=fooevents-settings&tab=checkins_app" class="nav-tab <?php echo 'checkins_app' === $active_tab ? 'nav-tab-active' : ''; ?>"><?php esc_attr_e( 'Check-ins App', 'woocommerce-events' ); ?></a>
		<a href="?page=fooevents-settings&tab=integration" class="nav-tab <?php echo 'integration' === $active_tab ? 'nav-tab-active' : ''; ?>"><?php esc_attr_e( 'Integration', 'woocommerce-events' ); ?></a>
	</h2>
	<form method="post" action="options.php">
		<table class="form-table fooevents-settings">
			<?php if ( 'api' === $active_tab ) : ?>
				<?php settings_fields( 'fooevents-settings-api' ); ?>
				<?php do_settings_sections( 'fooevents-settings-api' ); ?>
			<tr valign="top">
				<th scope="row"><h2><?php esc_attr_e( 'FooEvents License', 'woocommerce-events' ); ?></h2></th>
				<td></td>
				<td></td>
			</tr>
			<tr valign="top">
				<th scope="row"><?php esc_attr_e( 'FooEvents license key', 'woocommerce-events' ); ?></th>
				<td>
					<input type="password" name="globalWooCommerceEventsAPIKey" id="globalWooCommerceEventsAPIKey" value="<?php echo esc_html( $global_woocommerce_events_api_key ); ?>">
					<img class="help_tip fooevents-tooltip" title="<?php esc_attr_e( 'Required for automatic plugin updates. Leave empty if purchase was made on CodeCanyon.net AND no other plugin purchases were made on FooEvents.com. You must paste your license key here if any plugin purchases were made on FooEvents.com.', 'woocommerce-events' ); ?>" src="<?php echo esc_attr( plugins_url() ); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
				</td>
			</tr>  
			<tr valign="top">
				<th scope="row"><?php esc_attr_e( 'Envato purchase code', 'woocommerce-events' ); ?></th>
				<td>
					<input type="password" name="globalWooCommerceEnvatoAPIKey" id="globalWooCommerceEnvatoAPIKey" value="<?php echo esc_html( $global_woocommerce_envato_api_key ); ?>">
					<img class="help_tip fooevents-tooltip" title="<?php esc_attr_e( 'Required for automatic plugin updates. Leave empty if purchase was made on FooEvents.com AND no purchases were made on CodeCanyon.net. You must paste your Envato purchase code here if the FooEvents for WooCommerce plugin was purchased on CodeCanyon.net.', 'woocommerce-events' ); ?>" src="<?php echo esc_attr( plugins_url() ); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
				</td>
			</tr> 
			<?php endif; ?>
			<?php if ( 'general' === $active_tab ) : ?>
				<?php settings_fields( 'fooevents-settings-general' ); ?>
				<?php do_settings_sections( 'fooevents-settings-general' ); ?>
			<tr valign="top">
				<th scope="row"><h2><?php esc_attr_e( 'General', 'woocommerce-events' ); ?></h2></th>
				<td></td>
				<td></td>
			</tr>
			<tr valign="top">
				<th scope="row"><?php esc_attr_e( 'Change add to cart text', 'woocommerce-events' ); ?></th>
				<td>
					<input type="checkbox" name="globalWooCommerceEventsChangeAddToCart" id="globalWooCommerceEventsChangeAddToCart" value="yes" <?php echo ( 'yes' === $global_woocommerce_events_change_add_to_cart ) ? 'CHECKED' : ''; ?>>
					<img class="help_tip fooevents-tooltip" title="<?php esc_attr_e( "Changes 'Add to cart' text to 'Book ticket' for event products.", 'woocommerce-events' ); ?>" src="<?php echo esc_attr( plugins_url() ); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
				</td>
			</tr> 
			<tr valign="top">
				<th scope="row"><?php esc_attr_e( 'Enable event sorting options', 'woocommerce-events' ); ?></th>
				<td>
					<input type="checkbox" name="globalWooCommerceEventSorting" id="globalWooCommerceEventSorting" value="yes" <?php echo ( 'yes' === $global_woocommerce_event_sorting ) ? 'CHECKED' : ''; ?>>
					<img class="help_tip fooevents-tooltip" title="<?php esc_attr_e( 'Adds sort by date options to the WooCommerce product sorting drop-down list. You can set the default sort option in the WordPress Customizer.', 'woocommerce-events' ); ?>" src="<?php echo esc_attr( plugins_url() ); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
				</td>
			</tr> 
			<tr valign="top">
				<th scope="row"><?php esc_attr_e( 'Display event date on product listings', 'woocommerce-events' ); ?></th>
				<td>
					<input type="checkbox" name="globalWooCommerceDisplayEventDate" id="globalWooCommerceDisplayEventDate" value="yes" <?php echo ( 'yes' === $global_woocommerce_display_event_date ) ? 'CHECKED' : ''; ?>>
					<img class="help_tip fooevents-tooltip" title="<?php esc_attr_e( 'Adds the event date above the product title on product listing pages.', 'woocommerce-events' ); ?>" src="<?php echo esc_attr( plugins_url() ); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
				</td>
			</tr> 
			<tr valign="top">
				<th scope="row"><?php esc_attr_e( 'Hide event details tab', 'woocommerce-events' ); ?></th>
				<td>
					<input type="checkbox" name="globalWooCommerceHideEventDetailsTab" id="globalWooCommerceHideEventDetailsTab" value="yes" <?php echo ( 'yes' === $global_woocommerce_hide_event_details_tab ) ? 'CHECKED' : ''; ?>>
					<img class="help_tip fooevents-tooltip" title="<?php esc_attr_e( 'Hides the event details tab on the product page.', 'woocommerce-events' ); ?>" src="<?php echo esc_attr( plugins_url() ); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
				</td>
			</tr> 
			<tr valign="top">
				<th scope="row"><?php esc_attr_e( 'Use placeholders on checkout form', 'woocommerce-events' ); ?></th>
				<td>
					<input type="checkbox" name="globalWooCommerceUsePlaceHolders" id="globalWooCommerceUsePlaceHolders" value="yes" <?php echo ( 'yes' === $global_woocommerce_use_place_holders ) ? 'CHECKED' : ''; ?>>
					<img class="help_tip fooevents-tooltip" title="<?php esc_attr_e( "Displays placeholders in the checkout form (useful for themes that don't support form labels).", 'woocommerce-events' ); ?>" src="<?php echo esc_attr( plugins_url() ); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
				</td>
			</tr>
			<tr valign="top">
				<th scope="row"><?php esc_attr_e( 'Hide unpaid tickets', 'woocommerce-events' ); ?></th>
				<td>
					<input type="checkbox" name="globalWooCommerceEventsHideUnpaidTickets" id="globalWooCommerceEventsHideUnpaidTickets" value="yes" <?php echo ( 'yes' === $global_woocommerce_events_hide_unpaid_tickets ) ? 'CHECKED' : ''; ?>>
					<img class="help_tip fooevents-tooltip" title="<?php esc_attr_e( 'Hides unpaid tickets in ticket admin.', 'woocommerce-events' ); ?>" src="<?php echo esc_attr( plugins_url() ); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
				</td>
			</tr> 
			<tr valign="top">
				<th scope="row"><?php esc_attr_e( 'Email copy of ticket to other recipients', 'woocommerce-events' ); ?></th>
				<td>
					<input type="textbox" name="globalWooCommerceEventsEmailTicketAdmin" id="globalWooCommerceEventsEmailTicketAdmin" value="<?php echo esc_attr( $global_woocommerce_events_email_ticket_admin ); ?>" autocomplete="off">
					<img class="help_tip fooevents-tooltip" title="<?php esc_attr_e( 'Sends a copy of every ticket email to the specified email address. Use a comma separated list to send to multiple email addresses.', 'woocommerce-events' ); ?>" src="<?php echo esc_attr( plugins_url() ); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
				</td>
			</tr>
			<tr valign="top">
				<th scope="row"><?php esc_attr_e( "Copy ticket purchaser's details", 'woocommerce-events' ); ?></th>
				<td>
					<select name="globalWooCommerceEventsAddCopyPurchaserDetails" id="globalWooCommerceEventsAddCopyPurchaserDetails">
						<option value=""><?php esc_attr_e( 'Disabled', 'woocommerce-events' ); ?></option>
						<option value="icon" <?php echo ( 'yes' === $global_woocommerce_events_add_copy_purchaser_details || 'icon' === $global_woocommerce_events_add_copy_purchaser_details ) ? 'SELECTED' : ''; ?>><?php esc_attr_e( 'Icon', 'woocommerce-events' ); ?></option>
						<option value="text" <?php echo ( 'text' === $global_woocommerce_events_add_copy_purchaser_details ) ? 'SELECTED' : ''; ?>><?php esc_attr_e( 'Text', 'woocommerce-events' ); ?></option>
						<option value="textandicon" <?php echo ( 'textandicon' === $global_woocommerce_events_add_copy_purchaser_details ) ? 'SELECTED' : ''; ?>><?php esc_attr_e( 'Icon and Text', 'woocommerce-events' ); ?></option>
					</select>
					<img class="help_tip fooevents-tooltip" title="<?php esc_attr_e( "Adds the option to copy across the ticket purchaser's details for the attendee's first name, last name and email address on the checkout page using either an icon, text, or icon and text visual prompt.", 'woocommerce-events' ); ?>" src="<?php echo esc_attr( plugins_url() ); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
				</td>
			</tr>
			<tr valign="top">
				<th scope="row"><?php esc_attr_e( 'Event expiration method', 'woocommerce-events' ); ?></th>
				<td>
					<select name="globalWooCommerceEventsExpireOption">
						<option value="disable" <?php echo ( 'disable' === $global_woocommerce_events_expire_option ) ? 'SELECTED' : ''; ?>><?php esc_attr_e( 'Disable purchase', 'woocommerce-events' ); ?></option>
						<option value="hide" <?php echo ( 'hide' === $global_woocommerce_events_expire_option ) ? 'SELECTED' : ''; ?>><?php esc_attr_e( 'Hide product', 'woocommerce-events' ); ?></option>
					</select>
					<img class="help_tip fooevents-tooltip" title="<?php esc_attr_e( 'Disable the ability to purchase the ticket/product or hide it in page listings after the event has expired.', 'woocommerce-events' ); ?>" src="<?php echo esc_attr( plugins_url() ); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
				</td>
			</tr>
			<?php endif; ?>
			<?php if ( 'terminology' === $active_tab ) : ?>
				<?php settings_fields( 'fooevents-settings-terminology' ); ?>
				<?php do_settings_sections( 'fooevents-settings-terminology' ); ?>
			<tr valign="top">
				<th scope="row"><h2><?php esc_attr_e( 'Terminology', 'woocommerce-events' ); ?></h2></th>
				<td></td>
				<td width="100%"></td>
			</tr>
			<tr valign="top">
				<th scope="row"></th>
				<td><?php esc_attr_e( 'Singular', 'woocommerce-events' ); ?></td>
				<td><?php esc_attr_e( 'Plural', 'woocommerce-events' ); ?></td>
			</tr>
			<tr valign="top">
				<th scope="row"><?php esc_attr_e( 'Event', 'woocommerce-events' ); ?></th>
				<td>
					<input type="text" name="globalWooCommerceEventsEventOverride" id="globalWooCommerceEventsEventOverride" value="<?php echo esc_attr( $global_woocommerce_events_event_override ); ?>">
				</td>
				<td>
					<input type="text" name="globalWooCommerceEventsEventOverridePlural" id="globalWooCommerceEventsEventOverridePlural" value="<?php echo esc_attr( $global_woocommerce_events_event_override_plural ); ?>">
					<img class="help_tip fooevents-tooltip" title="<?php esc_attr_e( "Change 'event' to your own custom text.", 'woocommerce-events' ); ?>" src="<?php echo esc_attr( plugins_url() ); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
				</td>
			</tr>
			<tr valign="top">
				<th scope="row"><?php esc_attr_e( 'Attendee', 'woocommerce-events' ); ?></th>
				<td>
					<input type="text" name="globalWooCommerceEventsAttendeeOverride" id="globalWooCommerceEventsAttendeeOverride" value="<?php echo esc_attr( $global_woocommerce_events_attendee_override ); ?>">
				</td>
				<td>
					<input type="text" name="globalWooCommerceEventsAttendeeOverridePlural" id="globalWooCommerceEventsAttendeeOverridePlural" value="<?php echo esc_attr( $global_woocommerce_events_attendee_override_plural ); ?>">
					<img class="help_tip fooevents-tooltip" title="<?php esc_attr_e( "Change 'attendee' to your own custom text.", 'woocommerce-events' ); ?>" src="<?php echo esc_attr( plugins_url() ); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
				</td>
			</tr> 
			<tr valign="top">
				<th scope="row"><?php esc_attr_e( 'Book ticket', 'woocommerce-events' ); ?></th>
				<td>
					<input type="text" name="globalWooCommerceEventsTicketOverride" id="globalWooCommerceEventsTicketOverride" value="<?php echo esc_attr( $global_woocommerce_events_ticket_override ); ?>">
				</td>
				<td>
					<input type="text" name="globalWooCommerceEventsTicketOverridePlural" id="globalWooCommerceEventsTicketOverridePlural" value="<?php echo esc_attr( $global_woocommerce_events_ticket_override_plural ); ?>">
					<img class="help_tip fooevents-tooltip" title="<?php esc_attr_e( "Change 'Book ticket' to your own custom text.", 'woocommerce-events' ); ?>" src="<?php echo esc_attr( plugins_url() ); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
				</td>
			</tr> 
			<tr valign="top">
				<th scope="row"><?php esc_attr_e( 'Day', 'woocommerce-events' ); ?></th>
				<td>
					<input type="text" name="WooCommerceEventsDayOverride" id="WooCommerceEventsDayOverride" value="<?php echo esc_attr( $woocommerce_events_day_override ); ?>">
				</td>
				<td>
					<input type="text" name="WooCommerceEventsDayOverridePlural" id="WooCommerceEventsDayOverridePlural" value="<?php echo esc_attr( $woocommerce_events_day_override_plural ); ?>">
					<img class="help_tip fooevents-tooltip" title="<?php esc_attr_e( "Change 'Copy' to your own custom text.", 'woocommerce-events' ); ?>" src="<?php echo esc_attr( plugins_url() ); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
				</td>
			</tr>
			<tr valign="top">
				<th scope="row"><?php esc_attr_e( 'Copy', 'woocommerce-events' ); ?></th>
				<td>
					<input type="text" name="WooCommerceEventsCopyOverride" id="WooCommerceEventsCopyOverride" value="<?php echo esc_attr( $woocommerce_events_copy_override ); ?>">
				</td>
				<td>
					<input type="text" name="WooCommerceEventsCopyOverridePlural" id="WooCommerceEventsCopyOverridePlural" value="<?php echo esc_attr( $woocommerce_events_copy_override_plural ); ?>">
					<img class="help_tip fooevents-tooltip" title="<?php esc_attr_e( "Change 'Copy' to your own custom text.", 'woocommerce-events' ); ?>" src="<?php echo esc_attr( plugins_url() ); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
				</td>
			</tr>
			<?php endif; ?>
			<?php if ( 'ticket_design' === $active_tab ) : ?>
				<?php settings_fields( 'fooevents-settings-ticket-design' ); ?>
				<?php do_settings_sections( 'fooevents-settings-ticket-design' ); ?>
			<tr valign="top">
				<th scope="row"><h2><?php esc_attr_e( 'Ticket Design', 'woocommerce-events' ); ?></h2></th>
				<td></td>
				<td></td>
			</tr>
			<tr valign="top">
				<th scope="row"><?php esc_attr_e( 'Global ticket border', 'woocommerce-events' ); ?></th>
				<td>
					<input type="text" name="globalWooCommerceEventsTicketBackgroundColor" id="globalWooCommerceEventsTicketBackgroundColor" class="woocommerce-events-color-field" value="<?php echo esc_html( $global_woocommerce_events_ticket_background_color ); ?>">
					<img class="help_tip fooevents-tooltip" title="<?php esc_attr_e( 'Color of the ticket border.', 'woocommerce-events' ); ?>" src="<?php echo esc_attr( plugins_url() ); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
				</td>
			</tr>
			<tr valign="top">
				<th scope="row"><?php esc_attr_e( 'Global ticket button', 'woocommerce-events' ); ?></th>
				<td>
					<input type="text" name="globalWooCommerceEventsTicketButtonColor" id="globalWooCommerceEventsTicketButtonColor" class="woocommerce-events-color-field" value="<?php echo esc_html( $global_woocommerce_events_ticket_button_color ); ?>">
					<img class="help_tip fooevents-tooltip" title="<?php esc_attr_e( 'Color of the ticket button.', 'woocommerce-events' ); ?>" src="<?php echo esc_attr( plugins_url() ); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
				</td>
			</tr>
			<tr valign="top">
				<th scope="row"><?php esc_attr_e( 'Global ticket button text', 'woocommerce-events' ); ?></th>
				<td>
					<input type="text" name="globalWooCommerceEventsTicketTextColor" id="globalWooCommerceEventsTicketTextColor" class="woocommerce-events-color-field" value="<?php echo esc_html( $global_woocommerce_events_ticket_text_color ); ?>">
					<img class="help_tip fooevents-tooltip" title="<?php esc_attr_e( 'Color of the text in the ticket button.', 'woocommerce-events' ); ?>" src="<?php echo esc_attr( plugins_url() ); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
				</td>
			</tr>  
			<tr valign="top">
				<th scope="row"><?php esc_attr_e( 'Global ticket logo', 'woocommerce-events' ); ?></th>
				<td>
					<input id="globalWooCommerceEventsTicketLogo" class="text uploadfield" type="text" size="40" name="globalWooCommerceEventsTicketLogo" value="<?php echo esc_attr( $global_woocommerce_events_ticket_logo ); ?>" />                
					<span class="uploadbox">
						<input class="upload_image_button_woocommerce_events  button  " type="button" value="Upload file" />
						<a href="#" class="upload_reset"><?php esc_attr_e( 'Clear', 'woocommerce-events' ); ?></a>
						<img class="help_tip fooevents-tooltip" title="<?php esc_attr_e( 'Full URL that links to the logo that will be used in the ticket.', 'woocommerce-events' ); ?>" src="<?php echo esc_attr( plugins_url() ); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
					</span>
				</td>
			</tr>  
			<tr valign="top">
				<th scope="row"><?php esc_attr_e( 'Global ticket header image', 'woocommerce-events' ); ?></th>
				<td>
					<input id="globalWooCommerceEventsTicketHeaderImage" class="text uploadfield" type="text" size="40" name="globalWooCommerceEventsTicketHeaderImage" value="<?php echo esc_attr( $global_woocommerce_events_ticket_header_image ); ?>" />               
					<span class="uploadbox">
						<input class="upload_image_button_woocommerce_events  button  " type="button" value="Upload file" />
						<a href="#" class="upload_reset"><?php esc_attr_e( 'Clear', 'woocommerce-events' ); ?></a>
						<img class="help_tip fooevents-tooltip" title="<?php esc_attr_e( 'Full URL that links to the image that will be used as the ticket header.', 'woocommerce-events' ); ?>" src="<?php echo esc_attr( plugins_url() ); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
					</span>
				</td>
			</tr>
			<tr valign="top">
				<th scope="row"><?php esc_attr_e( 'Enable QR codes', 'woocommerce-events' ); ?></th>
				<td>
					<input type="hidden" name="globalWooCommerceEventsEnableQRCode" id="globalWooCommerceEventsEnableQRCode" value="no">
					<input type="checkbox" name="globalWooCommerceEventsEnableQRCode" id="globalWooCommerceEventsEnableQRCode" value="yes" <?php echo ( 'yes' === $global_woocommerce_events_enable_qr_code ) ? 'CHECKED' : ''; ?>>
					<img class="help_tip fooevents-tooltip" title="<?php esc_attr_e( 'Use QR codes instead of 1D barcodes on tickets.', 'woocommerce-events' ); ?>" src="<?php echo esc_attr( plugins_url() ); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
				</td>
			</tr> 
			<?php endif; ?>

			<?php if ( 'pdf' === $active_tab ) : ?>
				<?php echo $pdf_options; ?>
			<?php endif; ?>
			<?php if ( 'calendar' === $active_tab ) : ?>
				<?php echo $calendar_options; ?>
			<?php endif; ?>
			<?php if ( 'seating' === $active_tab ) : ?>
				<?php echo $seating_options; ?>
			<?php endif; ?>
			<?php if ( 'checkins_app' === $active_tab ) : ?>
				<?php settings_fields( 'fooevents-settings-checkins-app' ); ?>
				<?php do_settings_sections( 'fooevents-settings-checkins-app' ); ?>
			<tr valign="top">
				<th scope="row"><h2><?php esc_attr_e( 'Check-ins App', 'woocommerce-events' ); ?></h2></th>
				<td></td>
				<td></td>
			</tr>
			<tr valign="top">
				<th scope="row"><?php esc_attr_e( 'Hide personal information', 'woocommerce-events' ); ?></th>
				<td>
					<input type="checkbox" name="globalWooCommerceEventsAppHidePersonalInfo" id="globalWooCommerceEventsAppHidePersonalInfo" value="yes" <?php echo ( 'yes' === $global_woocommerce_events_app_hide_personal_info ) ? 'CHECKED' : ''; ?>>
					<img class="help_tip fooevents-tooltip" title="<?php esc_attr_e( 'Hides all personal information for attendees and/or ticket purchasers in the app. Only attendee names will be visible for check-in purposes.', 'woocommerce-events' ); ?>" src="<?php echo esc_attr( plugins_url() ); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
				</td>
			</tr> 
			<tr valign="top">
				<th scope="row"><?php esc_attr_e( 'Hide unpaid tickets', 'woocommerce-events' ); ?></th>
				<td>
					<input type="checkbox" name="globalWooCommerceHideUnpaidTicketsApp" id="globalWooCommerceHideUnpaidTicketsApp" value="yes" <?php echo ( 'yes' === $global_woocommerce_hide_unpaid_tickets_app ) ? 'CHECKED' : ''; ?>>
					<img class="help_tip fooevents-tooltip" title="<?php esc_attr_e( 'Hides all unpaid tickets in the app.', 'woocommerce-events' ); ?>" src="<?php echo esc_attr( plugins_url() ); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
				</td>
			</tr> 
			<tr valign="top">
				<th scope="row"><?php esc_attr_e( 'App title', 'woocommerce-events' ); ?></th>
				<td>
					<input type="text" name="globalWooCommerceEventsAppTitle" id="globalWooCommerceEventsAppTitle" placeholder="<?php esc_attr_e( 'e.g. Attendee Check-ins', 'woocommerce-events' ); ?>" class="text" size="40" value="<?php echo esc_attr( $global_woocommerce_events_app_title ); ?>">
					<img class="help_tip fooevents-tooltip" title="<?php esc_attr_e( 'The title that displays on the app sign-in screen beneath the app logo.', 'woocommerce-events' ); ?>" src="<?php echo esc_attr( plugins_url() ); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
				</td>
			</tr> 
			<tr valign="top">
				<th scope="row"><?php esc_attr_e( 'App logo', 'woocommerce-events' ); ?></th>
				<td>
					<input id="globalWooCommerceEventsAppLogo" class="text uploadfield" type="text" size="40" name="globalWooCommerceEventsAppLogo" value="<?php echo esc_attr( $global_woocommerce_events_app_logo ); ?>" />             
					<span class="uploadbox">
						<input class="upload_image_button_woocommerce_events  button  " type="button" value="Upload file" />
						<a href="#" class="upload_reset"><?php esc_attr_e( 'Clear', 'woocommerce-events' ); ?></a>
						<img class="help_tip fooevents-tooltip" title="<?php esc_attr_e( 'Full URL that links to the image that will be used as the logo on the sign-in screen (PNG format with transparency and a width of around 940px is recommended).', 'woocommerce-events' ); ?>" src="<?php echo esc_attr( plugins_url() ); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
					</span>
				</td>
			</tr> 
			<tr valign="top">
				<th scope="row"><?php esc_attr_e( 'Accent color', 'woocommerce-events' ); ?></th>
				<td>
					<input type="text" name="globalWooCommerceEventsAppColor" id="globalWooCommerceEventsAppColor" class="woocommerce-events-color-field" value="<?php echo esc_attr( $global_woocommerce_events_app_color ); ?>">
					<img class="help_tip fooevents-tooltip" title="<?php esc_attr_e( 'Color of the top navigation bar and sign-in button.', 'woocommerce-events' ); ?>" src="<?php echo esc_attr( plugins_url() ); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
				</td>
			</tr> 
			<tr valign="top">
				<th scope="row"><?php esc_attr_e( 'Accent text color', 'woocommerce-events' ); ?></th>
				<td>
					<input type="text" name="globalWooCommerceEventsAppTextColor" id="globalWooCommerceEventsAppTextColor" class="woocommerce-events-color-field" value="<?php echo esc_attr( $global_woocommerce_events_app_text_color ); ?>">
					<img class="help_tip fooevents-tooltip" title="<?php esc_attr_e( 'Color of the text in the top navigation bar and sign-in button.', 'woocommerce-events' ); ?>" src="<?php echo esc_attr( plugins_url() ); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
				</td>
			</tr> 
			<tr valign="top">
				<th scope="row"><?php esc_attr_e( 'Background color', 'woocommerce-events' ); ?></th>
				<td>
					<input type="text" name="globalWooCommerceEventsAppBackgroundColor" id="globalWooCommerceEventsAppBackgroundColor" class="woocommerce-events-color-field" value="<?php echo esc_attr( $global_woocommerce_events_app_background_color ); ?>">
					<img class="help_tip fooevents-tooltip" title="<?php esc_attr_e( 'Color of the background on the sign-in screen.', 'woocommerce-events' ); ?>" src="<?php echo esc_attr( plugins_url() ); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
				<td>
			</tr> 
			<tr valign="top">
				<th scope="row"><?php esc_attr_e( 'Title text color', 'woocommerce-events' ); ?></th>
				<td>
					<input type="text" name="globalWooCommerceEventsAppSignInTextColor" id="globalWooCommerceEventsAppSignInTextColor" class="woocommerce-events-color-field" value="<?php echo esc_attr( $global_woocommerce_events_app_sign_in_text_color ); ?>">
					<img class="help_tip fooevents-tooltip" title="<?php esc_attr_e( 'Color of the title text beneath the logo on the sign-in screen.', 'woocommerce-events' ); ?>" src="<?php echo esc_attr( plugins_url() ); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
				</td>
			</tr> 
			<tr valign="top">
				<th scope="row"><?php esc_attr_e( 'Event listing options', 'woocommerce-events' ); ?></th>
				<td>
					<label><input type="radio" name="globalWooCommerceEventsAppEvents" id="globalWooCommerceEventsAppEventsAll" value="all" <?php echo ( 'all' === $global_woocommerce_events_app_events || empty( $global_woocommerce_events_app_events ) ) ? 'CHECKED' : ''; ?>> <?php esc_attr_e( 'Show all events', 'woocommerce-events' ); ?></label>
					<img class="help_tip fooevents-tooltip" title="<?php esc_attr_e( 'Manage how events are listed in the app. Changes can be made in real-time without the user needing to sign-out.', 'woocommerce-events' ); ?>" src="<?php echo esc_attr( plugins_url() ); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
					<br/><br/>
					<label><input type="radio" name="globalWooCommerceEventsAppEvents" id="globalWooCommerceEventsAppEventsUser" value="user" <?php echo ( 'user' === $global_woocommerce_events_app_events ) ? 'CHECKED' : ''; ?>> <?php esc_attr_e( 'Only show events created by the signed-in user', 'woocommerce-events' ); ?></label>
					<br/><br/>
					<label><input type="radio" name="globalWooCommerceEventsAppEvents" id="globalWooCommerceEventsAppEventsID" value="id" <?php echo ( 'id' === $global_woocommerce_events_app_events ) ? 'CHECKED' : ''; ?>> <?php esc_attr_e( 'Only show the following events:', 'woocommerce-events' ); ?></label>
					<br/><br/>
					<select name="globalWooCommerceEventsAppEventIDs[]" id="globalWooCommerceEventsAppEventIDs" multiple class="fooevents-multiselect" <?php echo ( 'id' !== $global_woocommerce_events_app_events ) ? 'disabled' : ''; ?>>
						<?php
						foreach ( $woocommerce_events_app_events as $woocommerce_events_app_event ) {
							?>
								<option value="<?php echo esc_attr( $woocommerce_events_app_event->ID ); ?>" <?php echo ! empty( $global_woocommerce_events_app_event_ids ) && in_array( $woocommerce_events_app_event->ID, $global_woocommerce_events_app_event_ids, true ) ? 'SELECTED' : ''; ?>><?php echo esc_attr( $woocommerce_events_app_event->post_title ); ?></option>
							<?php
						}
						?>
					</select>
				</td>
			</tr> 
			<?php endif; ?>

			<?php if ( 'integration' === $active_tab ) : ?>
				<?php settings_fields( 'fooevents-settings-integration' ); ?>
				<?php do_settings_sections( 'fooevents-settings-integration' ); ?>
			<tr valign="top">
				<th scope="row" colspan="3"><h3 class="fooevents-settings-section-title"><?php esc_attr_e( 'Google Maps', 'woocommerce-events' ); ?></h3></th>
			</tr>
			<tr valign="top">
				<th scope="row"><?php esc_attr_e( 'Google Maps API key', 'woocommerce-events' ); ?></th>
				<td>
					<input type="text" name="globalWooCommerceEventsGoogleMapsAPIKey" id="globalWooCommerceEventsGoogleMapsAPIKey" value="<?php echo esc_html( $global_woocommerce_events_google_maps_api_key ); ?>">
					<img class="help_tip fooevents-tooltip" title="<?php esc_attr_e( 'Enable Google Maps to be displayed on the product page.', 'woocommerce-events' ); ?>" src="<?php echo esc_attr( plugins_url() ); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
				</td>
			</tr>
			<tr valign="top">
				<th scope="row" colspan="3"><h3 class="fooevents-settings-section-title"><?php esc_attr_e( 'Zoom Meetings and Webinars', 'woocommerce-events' ); ?></h3></th>
			</tr>
			<tr valign="top">
				<th scope="row"><?php esc_attr_e( 'API Key', 'woocommerce-events' ); ?></th>
				<td>
					<input type="text" name="globalWooCommerceEventsZoomAPIKey" id="globalWooCommerceEventsZoomAPIKey" value="<?php echo esc_attr( $global_woocommerce_events_zoom_api_key ); ?>">
					<img class="help_tip fooevents-tooltip" title="<?php esc_attr_e( 'Required to securely connect to your Zoom account in order to register attendees for your meetings/webinars.', 'woocommerce-events' ); ?>" src="<?php echo esc_attr( plugins_url() ); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
				</td>
			</tr> 
			<tr valign="top">
				<th scope="row"><?php esc_attr_e( 'API Secret', 'woocommerce-events' ); ?></th>
				<td>
					<input type="password" name="globalWooCommerceEventsZoomAPISecret" id="globalWooCommerceEventsZoomAPISecret" value="<?php echo esc_attr( $global_woocommerce_events_zoom_api_secret ); ?>">
					<img class="help_tip fooevents-tooltip" title="<?php esc_attr_e( 'Required to securely connect to your Zoom account in order to register attendees for your meetings/webinars.', 'woocommerce-events' ); ?>" src="<?php echo esc_attr( plugins_url() ); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
				</td>
			</tr> 
			<tr valign="top">
				<th scope="row"></th>
				<td>
					<input id="fooevents_zoom_test_access" type="button" value="<?php esc_attr_e( 'Test Access', 'woocommerce-events' ); ?>" class="button button-secondary">
					<br/>
					<br/>
					<a href="https://help.fooevents.com/docs/topics/events/zoom-meetings-and-webinars/#generating-a-zoom-api-key-and-secret" target="_blank"><?php esc_attr_e( 'Get help generating Zoom API keys', 'woocommerce-events' ); ?></a>
				</td>
			</tr> 
			<tr valign="top" id="globalWooCommerceEventsZoomUsers" 
				<?php
				if ( empty( $global_woocommerce_events_zoom_api_key ) || empty( $global_woocommerce_events_zoom_api_secret ) ) :
					?>
				style="display:none;"<?php endif; ?>>
				<th scope="row"><?php esc_attr_e( 'Users/Hosts', 'woocommerce-events' ); ?></th>
				<td>
					<input id="fooevents_zoom_fetch_users" type="button" value="<?php esc_attr_e( 'Fetch Users', 'woocommerce-events' ); ?>" class="button button-secondary">
					<img class="help_tip fooevents-tooltip" title="<?php esc_attr_e( 'Displays meetings/webinars on the Event Integration tab according to which users created them so that they can be linked to specific events. The default setting will only display meetings/webinars for the user that generated the API Key and Secret. The second option is useful if you have multiple hosts on your Zoom account and you would like to restrict which meetings/webinars are visible (Hint: Shift-Click or Ctrl/Cmd+Click to select multiple hosts).', 'woocommerce-events' ); ?>" src="<?php echo esc_attr( plugins_url() ); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
					<br/>
					<br/>
					<div id="globalWooCommerceEventsZoomUsersContainer">
						<?php if ( empty( $global_woocommerce_events_zoom_users ) ) : ?>
							<input type="hidden" name="globalWooCommerceEventsZoomUsers" value="[]" />
							<input type="hidden" name="globalWooCommerceEventsZoomSelectedUsers[]" value="me" />
						<?php else : ?>
							<input type="hidden" name="globalWooCommerceEventsZoomUsers" value="<?php echo esc_attr( wp_json_encode( $global_woocommerce_events_zoom_users ) ); ?>" />
							<label><input type="radio" name="globalWooCommerceEventsZoomSelectedUserOption" id="globalWooCommerceEventsZoomSelectedUserOptionMe" value="me" <?php echo ( empty( $global_woocommerce_events_zoom_selected_user_option ) || 'me' === $global_woocommerce_events_zoom_selected_user_option ) ? 'CHECKED' : ''; ?>> <?php esc_attr_e( 'Show only meetings/webinars for the user that generated the API Key and Secret', 'woocommerce-events' ); ?></label>
							<br/><br/>
							<label><input type="radio" name="globalWooCommerceEventsZoomSelectedUserOption" id="globalWooCommerceEventsZoomSelectedUserOptionSelect" value="select" <?php echo ( 'select' === $global_woocommerce_events_zoom_selected_user_option ) ? 'CHECKED' : ''; ?>> <?php esc_attr_e( 'Show all meetings/webinars created by the following users:', 'woocommerce-events' ); ?></label>
							<br/><br/>
							<select name="globalWooCommerceEventsZoomSelectedUsers[]" id="globalWooCommerceEventsZoomSelectedUsers" multiple class="fooevents-multiselect" <?php echo ( 'select' !== $global_woocommerce_events_zoom_selected_user_option ) ? 'disabled' : ''; ?>>
								<?php
								foreach ( $global_woocommerce_events_zoom_users as $user ) {
									?>
										<option value="<?php echo esc_attr( $user['id'] ); ?>" <?php echo ! empty( $global_woocommerce_events_zoom_selected_users ) && in_array( $user['id'], $global_woocommerce_events_zoom_selected_users, true ) ? 'SELECTED' : ''; ?>><?php echo esc_attr( $user['first_name'] ) . ' ' . esc_attr( $user['last_name'] ) . ' - ' . esc_attr( $user['email'] ); ?></option>
									<?php
								}
								?>
							</select>
							<p><?php esc_attr_e( 'Please note that meeting/webinar load times will increase as more users are selected.', 'woocommerce-events' ); ?></p>
						<?php endif; ?>
					</div>                    
				</td>
			</tr> 
				<?php echo $eventbrite_options; ?>
				<?php if ( $subscriptions_enabled ) : ?>
			<tr valign="top">
				<th scope="row" colspan="3"><h3 class="fooevents-settings-section-title"><?php esc_attr_e( 'WooCommerce Subscriptions', 'woocommerce-events' ); ?></h3></th>
			</tr>
			<tr valign="top">
				<th scope="row"><?php esc_attr_e( 'Disable new ticket generation for subscription renewals', 'woocommerce-events' ); ?></th>
				<td>
					<input type="checkbox" name="globalWooCommerceEventsDisableSubTicketGen" id="globalWooCommerceEventsDisableSubTicketGen" value="yes" <?php echo ( 'yes' === $global_woocommerce_events_disable_sub_ticket_gen ) ? 'CHECKED' : ''; ?>>
					<img class="help_tip fooevents-tooltip" title="<?php esc_attr_e( 'Only create tickets on parent order completion. No tickets will be generated on order renewals.', 'woocommerce-events' ); ?>" src="<?php echo esc_attr( plugins_url() ); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
				</td>
			</tr> 
			<?php endif; ?>
			<?php endif; ?>
		</table>
		<?php submit_button(); ?>
	</form>
</div>
